package com.example.website_act_frontend.controller;

import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.FrontendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Course;
import com.example.website_act_frontend.model.News;
import com.example.website_act_frontend.model.Register;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.service.ClassService;
import com.example.website_act_frontend.service.CourseService;
import com.example.website_act_frontend.service.NewsService;
import com.example.website_act_frontend.service.RegisterService;
import com.example.website_act_frontend.service.UserService;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@Controller("GuestController")
public class GuestController {

  private final RegisterService registerService;
  private final FrontendUtils frontendUtils;
  private final CourseService courseService;
  private final ClassService classService;
  private final UserService userService;
  private final NewsService newsService;

  private final GenericMapper genericMapper;

  /**
   * Trang chủ trung tâm
   */
  @SneakyThrows
  @GetMapping("/")
  public String getHome(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setCommonModel(request, model, search, token, true, false);

    CompletableFuture<Map<?, ?>> resultGetListNews =
        CompletableFuture.supplyAsync(() ->
            newsService.getListNews(new Search(1, "ACTIVE"), 1, 4));

    CompletableFuture<Map<?, ?>> resultGetTopUsers =
        CompletableFuture.supplyAsync(() ->
            userService.getListTopUser(1, 10));

    List<User> topUsers =
        genericMapper.mapToListOfType((List<?>) resultGetTopUsers.get().get("topUsers"),
            User.class);
    List<News> listNews =
        genericMapper.mapToListOfType((List<?>) resultGetListNews.get().get("listNews"),
            News.class);
    model.addAttribute("choose", 1);
    model.addAttribute("topUsers", topUsers);
    model.addAttribute("listNews", listNews);
    return "/guest/home";
  }

  /**
   * Trang giới thiệu trung tâm
   */
  @SneakyThrows
  @GetMapping("/introduce-act")
  public String getIntroduceAct(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setCommonModel(request, model, search, token, false, false);

    Map<String, Object> result =
        newsService.getListNews(new Search("ACTIVE", 1), 1, 99);
    List<News> listIntroduce =
        genericMapper.mapToListOfType((List<?>) result.get("listNews"), News.class);

    model.addAttribute("choose", 2);
    model.addAttribute("listIntroduce", listIntroduce);
    model.addAttribute("defaultContent", listIntroduce.get(0).getContent());
    return "/guest/introduce-academy";
  }

  /**
   * Trang danh sách khóa học
   */
  @GetMapping("/list-course/page/{pageId}")
  public String getListCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 10;
    Map<String, Object> result = courseService.getListCourse(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/list-course/page/1";
    }

    frontendUtils.setCommonModel(request, model, search, token, false, true);
    model.addAttribute("choose", 3);
    List<Course> courses =
        genericMapper.mapToListOfType((List<?>) result.get("courses"), Course.class);
    if (!CollectionUtils.isEmpty(courses)) {
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("courses", courses);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("courses", null);
    }
    return "/guest/list-course";
  }

  /**
   * Trang thông tin chi tiết khóa học
   */
  @GetMapping("/course/{courseId}")
  public String getCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "courseId") int courseId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = courseService.getDetailCourse(courseId);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/list-course/page/1";
    } else {
      frontendUtils.setCommonModel(request, model, search, token, false, true);
      model.addAttribute("choose", 3);
      model.addAttribute("course", result.get("course"));
      return "/guest/detail-course";
    }
  }

  /**
   * Trang danh sách tin tức
   */
  @GetMapping("/list-news/page/{pageId}")
  public String getListNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 4;
    search.setTypeIdNot(1);
    search.setState("ACTIVE");
    Map<String, Object> result = newsService.getListNews(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/list-news/page/1";
    }

    frontendUtils.setCommonModel(request, model, search, token, false, false);
    model.addAttribute("choose", 4);
    List<News> listNews =
        genericMapper.mapToListOfType((List<?>) result.get("listNews"), News.class);
    if (!CollectionUtils.isEmpty(listNews)) {
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("listNews", listNews);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listNews", null);
    }
    return "/guest/list-news";
  }

  /**
   * Trang thông tin chi tiết tin tức
   */
  @GetMapping("/news/{newsId}")
  public String getNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "newsId") int newsId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.getDetailNews(newsId);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/list-news/page/1";
    } else {
      frontendUtils.setCommonModel(request, model, search, token, false, false);
      model.addAttribute("choose", 4);
      model.addAttribute("news", result.get("news"));
      return "/guest/detail-news";
    }
  }

  /**
   * Trang Đăng ký học
   */
  @GetMapping("/register")
  public String getRegister(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @ModelAttribute Register register,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setCommonModel(request, model, search, token, false, true);

    if (register == null) {
      register = new Register();
    }

    User user = (User) model.getAttribute("loggedUser");
    if (user != null && user.getRole().equals(RoleEnums.User.name())) {
      register.setStudent(user);
    } else {
      register.setStudent(new User());
    }

    model.addAttribute("choose", 5);
    model.addAttribute("register", register);
    return "/guest/register";
  }

  /**
   * Thực thi Đăng ký học
   */
  @PostMapping("/register")
  public String executeRegister(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @ModelAttribute Register register,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setCommonModel(request, model, search, token, false, true);
    Map<String, Object> result = registerService.register(register);
    model.addAttribute("choose", 5);
    model.addAttribute("register", register);
    model.addAttribute("state", result.get("state"));
    model.addAttribute("notification", result.get("message"));
    return "/guest/register";
  }

  /**
   * Trang danh sách lớp học
   */
  @GetMapping("/list-class/page/{pageId}")
  public String getListClass(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    if ("DOING".equals(search.getState())) {
      search.setHowToLearn("Online");
    }
    Map<String, Object> result = classService.getListClass(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      redirectAttributes.addFlashAttribute("search", search);
      return "redirect:/list-class/page/1";
    }

    frontendUtils.setCommonModel(request, model, search, token, false, false);
    model.addAttribute("choose", 6);
    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/guest/list-class";
  }

  /**
   * Lấy trang kết quả tìm kiếm theo phân loại (Khóa học, Tin tức)
   */
  @GetMapping("/search/page/{pageId}")
  public String getSearch(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setCommonModel(request, model, search, token, false, false);

    int size = 12;
    int page = 0;
    int numberResult = 0;
    Map<String, Object> result;

    if ("news".equals(search.getType())) {
      result = newsService.getListNews(search, pageId, size);
      List<News> listNews =
          genericMapper.mapToListOfType((List<?>) result.get("listNews"), News.class);
      if (listNews != null) {
        numberResult = (int) result.get("totalElements");
        page = (int) result.get("totalPages");
      }
      model.addAttribute("courses", null);
      model.addAttribute("listNews", listNews);
    } else {
      result = courseService.getListCourse(search, pageId, size);
      List<Course> courses =
          genericMapper.mapToListOfType((List<?>) result.get("courses"), Course.class);
      if (courses != null) {
        numberResult = (int) result.get("totalElements");
        page = (int) result.get("totalPages");
      }
      model.addAttribute("courses", courses);
      model.addAttribute("listNews", null);
    }

    model.addAttribute("page", page);
    model.addAttribute("pageId", pageId);
    model.addAttribute("numberResult", numberResult);
    model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
    return "/guest/search";
  }
}
