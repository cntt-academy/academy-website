package com.example.website_act_frontend.controller;

import static com.example.website_act_frontend.common.utils.FrontendUtils.standardizedNotification;

import com.example.website_act_frontend.common.constant.WebsiteConstant;
import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.FrontendUtils;
import com.example.website_act_frontend.common.utils.RedisUtils;
import com.example.website_act_frontend.model.ChangePassword;
import com.example.website_act_frontend.model.Login;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.service.AuthenticationService;
import com.example.website_act_frontend.service.UserService;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@RequestMapping("/authentication")
@Controller("AuthenticationController")
public class AuthenticationController {

  private final AuthenticationService authenticationService;
  private final FrontendUtils frontendUtils;
  private final UserService userService;

  /**
   * Trang login
   */
  @SneakyThrows
  @GetMapping("/login")
  public String getLogin(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification) {
    if (StringUtils.isBlank(notification)) {
      notification = (String) request.getSession().getAttribute("notification");
      request.getSession().setAttribute("notification", null);
    }
    model.addAttribute("avatar_academy", WebsiteConstant.URL_AVATAR_ACADEMY);
    model.addAttribute("login", new Login());
    model.addAttribute("notification", standardizedNotification(notification));
    return "/authentication/login";
  }

  /**
   * Thực thi login
   */
  @SneakyThrows
  @PostMapping("/login")
  public String doLogin(
      HttpServletResponse response,
      @ModelAttribute Login login,
      RedirectAttributes redirectAttributes) {
    Map<String, Object> result = authenticationService.login(login);
    if (!result.get("state").equals("success")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/authentication/login";
    } else {
      response.addCookie((Cookie) result.get("cookie"));
      if ("Abc@1234".equalsIgnoreCase(login.getPassword())) {
        redirectAttributes.addFlashAttribute("oldPassword", "Abc@1234");
        redirectAttributes.addFlashAttribute("notification",
            "Bạn đang đăng nhập lần đầu vào hệ thống <br> Xin hãy đổi mật khẩu để tiếp tục");
        return "redirect:/authentication/update-password";
      } else {
        if (RoleEnums.User.name().equalsIgnoreCase((String) result.get("role"))) {
          return "redirect:/user/person-info";
        } else {
          return "redirect:/collaborator/statistic/page/1";
        }
      }
    }
  }

  /**
   * Trang đăng ký
   */
  @SneakyThrows
  @GetMapping("/sign-up")
  public String getSignUp(
      Model model,
      @ModelAttribute("user") User user,
      @ModelAttribute("notification") String notification) {
    if (user == null) {
      user = new User();
    }
    model.addAttribute("avatar_academy", WebsiteConstant.URL_AVATAR_ACADEMY);
    model.addAttribute("user", user);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/authentication/sign-up";
  }

  /**
   * Thực thi đăng ký tài khoản
   */
  @SneakyThrows
  @PostMapping("/sign-up")
  public String executeSignUp(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("user") User user) {
    Map<String, Object> result = userService.createUser(user);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      redirectAttributes.addFlashAttribute("user", user);
      return "redirect:/authentication/sign-up";
    } else {
      redirectAttributes.addFlashAttribute("notification",
          "Đăng ký tài khoản thành công <br> Xin hãy kiểm tra email bạn đăng ký tài khoản");
      return "redirect:/authentication/login";
    }
  }

  /**
   * Trang đổi mật khẩu sau khi login lần đầu
   */
  @SneakyThrows
  @GetMapping("/update-password")
  public String getUpdatePassword(
      Model model,
      @ModelAttribute("oldPassword") String oldPassword,
      @ModelAttribute("notification") String notification) {
    if (!"Abc@1234".equals(oldPassword)) {
      return "redirect:/authentication/login";
    }

    ChangePassword changePassword = new ChangePassword();
    model.addAttribute("avatar_academy", WebsiteConstant.URL_AVATAR_ACADEMY);
    model.addAttribute("request", changePassword);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/authentication/update-password";
  }

  /**
   * Thực thi đổi mật khẩu sau khi login lần đầu
   */
  @PostMapping("/update-password")
  public String executeUpdatePassword(
      RedirectAttributes redirectAttributes,
      @ModelAttribute ChangePassword changePassword,
      @CookieValue(name = "token", defaultValue = "") String token) {
    changePassword.setOldPassword("Abc@1234");
    Map<String, Object> result = authenticationService.changePassword(changePassword, token);
    if (result.get("state").equals("success")) {
      redirectAttributes.addFlashAttribute(
          "notification",
          "Đổi mật khẩu thành công <br> Xin hãy đăng nhập lại");
      return "redirect:/authentication/login";
    } else {
      redirectAttributes.addFlashAttribute("oldPassword", "Abc@1234");
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/authentication/update-password";
    }
  }

  /**
   * Thực thi quên mật khẩu
   */
  @SneakyThrows
  @GetMapping("/forgetPassword")
  public String doForgetPassword(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("email") String email) {
    Map<String, Object> result = authenticationService.forgetPassword(email);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/authentication/login";
  }

  /**
   * Trang đổi mật khẩu
   */
  @GetMapping("/change-password")
  public String getChangePassword(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    ChangePassword changePassword = new ChangePassword();
    model.addAttribute("request", changePassword);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/authentication/change-password";
  }

  /**
   * Thực thi đổi mật khẩu
   */
  @PostMapping("/change-password")
  public String executeChangePassword(
      RedirectAttributes redirectAttributes,
      @ModelAttribute ChangePassword changePassword,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = authenticationService.changePassword(changePassword, token);
    if (result.get("state").equals("success")) {
      redirectAttributes.addFlashAttribute(
          "notification",
          "Đổi mật khẩu thành công <br> Xin hãy đăng nhập lại");
      return "redirect:/authentication/logout";
    } else {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/authentication/change-password";
    }
  }

  /**
   * Thực thi logout
   */
  @SneakyThrows
  @GetMapping("/logout")
  public String logout(
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    // Xóa thông tin tài khoản trong Redis
    String email = (String) request.getSession().getAttribute("email");
    if (StringUtils.isNotBlank(email)) {
      RedisUtils.delete(email);
    }

    // Call API logout
    authenticationService.logout(token);

    request.getSession().setAttribute("resetLogin", true);
    request.getSession().setAttribute("email", null);
    request.getSession().setAttribute("name", null);
    request.getSession().setAttribute("role", null);

    if (StringUtils.isBlank(notification)) {
      notification = "Bạn đã đăng xuất khỏi hệ thống";
    }
    redirectAttributes.addFlashAttribute("notification", notification);
    return "redirect:/authentication/login";
  }
}
