package com.example.website_act_frontend.controller;

import static com.example.website_act_frontend.common.utils.FrontendUtils.standardizedNotification;

import com.example.website_act_frontend.common.utils.FrontendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.model.VideoLecture;
import com.example.website_act_frontend.service.ClassService;
import com.example.website_act_frontend.service.UserService;
import com.example.website_act_frontend.service.VideoLectureService;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@RequestMapping("/user")
@Controller("UserController")
public class UserController {

  private final VideoLectureService videoLectureService;
  private final FrontendUtils frontendUtils;
  private final ClassService classService;
  private final UserService userService;

  private final GenericMapper genericMapper;

  /**
   * Trang thông tin chi tiết Tài khoản
   */
  @GetMapping("/person-info")
  public String getUserInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/user/person-info";
  }

  /**
   * Trang thông tin Quản lý trung tâm
   */
  @SneakyThrows
  @GetMapping("/manager-info")
  public String getManagerInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("manager", frontendUtils.getManagerInfo());
    return "/user/manager-info";
  }

  /**
   * Trang cập nhật thông tin
   */
  @SneakyThrows
  @GetMapping("/update")
  public String getUpdate(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    User user = frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("user", user);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/user/person-update";
  }

  /**
   * Thực thi cập nhật thông tin
   */
  @SneakyThrows
  @PostMapping("/update")
  public String executeUpdate(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("user") User user,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = userService.updateUser(user, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/user/update";
    } else {
      return "redirect:/user/person-info";
    }
  }

  /**
   * Trang danh sách các Lớp học
   */
  @SneakyThrows
  @GetMapping("/list-class/page/{pageId}")
  public String getListClass(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    User user = frontendUtils.setLoggedAccountModel(request, model, token);

    int size = 5;
    Map<String, Object> result =
        classService.getListClassByUserId(user.getId(), search, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/user/list-class/page/page/1";
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/user/list-class";
  }

  /**
   * Trang thông tin chi tiết Lớp học
   */
  @GetMapping("/class/{classId}/video/page/{pageId}")
  public String getDetailClass(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);

    Map<String, Object> result = classService.getDetailClass(classId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/user/list-class/page/1";
    }

    Clazz clazz = (Clazz) result.get("class");

    int size = 5;
    Map<String, Object> resultGetListVideoLecture =
        videoLectureService.getListVideoLecture(classId, search, pageId, size, token);
    int page = (int) resultGetListVideoLecture.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/user/class/%d/video/page/1", classId);
    }

    List<VideoLecture> videoLectures =
        genericMapper.mapToListOfType(
            (List<?>) resultGetListVideoLecture.get("videoLectures"), VideoLecture.class);
    model.addAttribute("class", clazz);
    model.addAttribute("search", search);
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(videoLectures)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("videoLectures", videoLectures);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("videoLectures", null);
    }
    return "/user/class-info";
  }

  /**
   * Trang thông tin chi tiết bài giảng
   */
  @GetMapping("/class/{classId}/video-lecture/{videoLectureId}")
  public String getDetailVideoLecture(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "videoLectureId") int videoLectureId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = videoLectureService.getDetailVideoLecture(videoLectureId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      String message = (String) result.get("message");
      redirectAttributes.addFlashAttribute("notification", message);
      return String.format("redirect:/user/class/%d/video/page/1/#videoLectures", classId);
    } else {
      CompletableFuture.runAsync(() -> videoLectureService.addHistoryWatch(videoLectureId, token));
      VideoLecture videoLecture = (VideoLecture) result.get("videoLecture");
      model.addAttribute("videoLecture", videoLecture);
      return "/user/video-lecture-info";
    }
  }
}
