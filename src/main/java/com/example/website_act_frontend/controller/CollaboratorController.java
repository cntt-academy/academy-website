package com.example.website_act_frontend.controller;

import static com.example.website_act_frontend.common.utils.FrontendUtils.standardizedNotification;

import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.FrontendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.model.Admin;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Course;
import com.example.website_act_frontend.model.News;
import com.example.website_act_frontend.model.RegisterOutput;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.StatisticTime;
import com.example.website_act_frontend.model.StudentStatistic;
import com.example.website_act_frontend.model.TypeCourse;
import com.example.website_act_frontend.model.TypeNews;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.model.VideoLecture;
import com.example.website_act_frontend.service.AdminService;
import com.example.website_act_frontend.service.ClassService;
import com.example.website_act_frontend.service.CourseService;
import com.example.website_act_frontend.service.NewsService;
import com.example.website_act_frontend.service.RegisterService;
import com.example.website_act_frontend.service.TeacherService;
import com.example.website_act_frontend.service.TypeCourseService;
import com.example.website_act_frontend.service.TypeNewsService;
import com.example.website_act_frontend.service.UserService;
import com.example.website_act_frontend.service.VideoLectureService;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@RequestMapping("/collaborator")
@Controller("CollaboratorController")
public class CollaboratorController {

  private final VideoLectureService videoLectureService;
  private final TypeCourseService typeCourseService;
  private final TypeNewsService typeNewsService;
  private final RegisterService registerService;
  private final TeacherService teacherService;
  private final CourseService courseService;
  private final FrontendUtils frontendUtils;
  private final ClassService classService;
  private final AdminService adminService;
  private final UserService userService;
  private final NewsService newsService;

  private final GenericMapper genericMapper;

  /**
   * Trang Home Cộng tác viên và Quản lý trung tâm
   */
  @SneakyThrows
  @GetMapping("/statistic/page/{pageId}")
  public String getStatistic(
      Model model,
      HttpServletRequest request,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute StatisticTime statisticTime,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    if (statisticTime.getFromDate() == null && statisticTime.getToDate() == null) {
      statisticTime = new StatisticTime(frontendUtils.getFirstDayOfYear(), LocalDate.now());
    }
    StatisticTime finalStatisticTime = statisticTime;

    CompletableFuture<Integer> totalStudent =
        CompletableFuture.supplyAsync(
            () -> {
              Map<String, Object> result =
                  registerService.countTotalStudent(finalStatisticTime, token);
              return (Integer) result.get("totalStudent");
            });

    CompletableFuture<Map<?, ?>> overviewStatistic =
        CompletableFuture.supplyAsync(
            () -> registerService.overviewStatistic(finalStatisticTime, token));

    int size = 5;
    CompletableFuture<Map<String, Object>> detailStatistic =
        CompletableFuture.supplyAsync(
            () -> registerService.detailStatistic(finalStatisticTime, pageId, size, token));

    if (StringUtils.isBlank(notification)) {
      notification = (String) request.getSession().getAttribute("notification");
      request.getSession().setAttribute("notification", null);
    }

    frontendUtils.setLoggedAccountModel(request, model, token);
    String role = (String) request.getSession().getAttribute("role");
    model.addAttribute("role", RoleEnums.valueOf(role).getValue());
    model.addAttribute("notification", standardizedNotification(notification));

    model.addAttribute("statisticTime", finalStatisticTime);
    model.addAttribute("totalStudent", totalStudent.get());

    Map<?, ?> map = overviewStatistic.get();
    model.addAttribute("maxStudent", frontendUtils.nearestTens(map.values()));
    model.addAttribute("overviewStatistic", overviewStatistic.get());

    Map<String, Object> result = detailStatistic.get();
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/statistic/page/1";
    }

    List<StudentStatistic> studentStatistics =
        genericMapper.mapToListOfType(
            (List<?>) result.get("studentStatistics"), StudentStatistic.class);
    if (!CollectionUtils.isEmpty(studentStatistics)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("detailStudentStatistics", studentStatistics);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("detailStudentStatistics", null);
    }

    return "/admin/admin-home";
  }

  /**
   * Trang thông tin Quản lý trung tâm
   */
  @SneakyThrows
  @GetMapping("/manager-info/class/page/{pageId}")
  public String getManagerInfo(
      Model model,
      HttpServletRequest request,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Admin manager = frontendUtils.getManagerInfo();

    int size = 5;
    Map<String, Object> result =
        classService.getListClassByAdminEmail(manager.getEmail(), pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/manager-info/class/page/1";
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("collaborator", manager);
    model.addAttribute("role", RoleEnums.Manager.getValue());
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/admin-info";
  }

  /**
   * Trang thông tin Cộng tác viên
   */
  @SneakyThrows
  @GetMapping("/person-info/class/page/{pageId}")
  public String getPersonInfo(
      Model model,
      HttpServletRequest request,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    User collaborator = frontendUtils.setLoggedAccountModel(request, model, token);

    int size = 5;
    Map<String, Object> result =
        classService.getListClassByAdminEmail(collaborator.getEmail(), pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/person-info/class/page/1";
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("collaborator", collaborator);
    model.addAttribute("role", RoleEnums.Collaborator.getValue());
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/admin-info";
  }

  /**
   * Trang cập nhật thông tin Cộng tác viên
   */
  @SneakyThrows
  @GetMapping("/update-collaborator/{collaboratorId}")
  public String getUpdateCollaborator(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "collaboratorId") int collaboratorId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.getCollaboratorInfo(collaboratorId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/collaborator-management/page/1";
    }

    frontendUtils.setLoggedAccountModel(request, model, token);
    Admin collaborator = (Admin) result.get("collaborator");
    model.addAttribute("collaborator", collaborator);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/collaborator-update";
  }

  /**
   * Thực thi cập nhật thông tin Cộng tác viên
   */
  @SneakyThrows
  @PostMapping("/update-collaborator")
  public String executeUpdateCollaborator(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("collaborator") Admin collaborator,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.updateAdmin(collaborator, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-collaborator/" + collaborator.getId();
    } else {
      return String.format("redirect:/collaborator/%d/class/page/1", collaborator.getId());
    }
  }

  // ---------------------------------- TYPE COURSE --------------------------------- //

  /**
   * Trang danh sách loại khóa học
   */
  @SneakyThrows
  @GetMapping("/type-course-management/page/{pageId}")
  public String getTypeCourseManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result =
        typeCourseService.getListTypeCourse(search, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/type-course-management/page/1";
    }

    List<TypeCourse> typeCourses =
        genericMapper.mapToListOfType((List<?>) result.get("typeCourses"), TypeCourse.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(typeCourses)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("typeCourses", typeCourses);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("typeCourses", null);
    }
    return "/admin/type-course-management";
  }

  /**
   * Trang thông tin chi tiết loại khóa học
   */
  @GetMapping("/type-course/{typeCourseId}")
  public String getDetailTypeCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "typeCourseId") int typeCourseId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = typeCourseService.getDetailTypeCourse(typeCourseId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/type-course-management/page/1";
    } else {
      TypeCourse typeCourse = (TypeCourse) result.get("typeCourse");
      model.addAttribute("typeCourse", typeCourse);
      return "/admin/type-course-info";
    }
  }

  // ---------------------------------- COURSE --------------------------------- //

  /**
   * Trang danh sách các Khóa học
   */
  @SneakyThrows
  @GetMapping("/course-management/page/{pageId}")
  public String getCourseManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = courseService.getListCourse(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/course-management/page/1";
    }

    List<Course> courses =
        genericMapper.mapToListOfType((List<?>) result.get("courses"), Course.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("typeCourses", frontendUtils.getAllTypeCourse());
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(courses)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("courses", courses);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("courses", null);
    }
    return "/admin/course-management";
  }

  /**
   * Trang thông tin chi tiết khóa học
   */
  @GetMapping("/course/{courseId}")
  public String getDetailCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "courseId") int courseId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = courseService.getDetailCourse(courseId);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/course-management/page/1";
    } else {
      Course course = (Course) result.get("course");
      model.addAttribute("course", course);
      return "/admin/course-info";
    }
  }

  // ---------------------------------- COLLABORATOR --------------------------------- //

  /**
   * Trang danh sách Cộng tác viên
   */
  @SneakyThrows
  @GetMapping("/collaborator-management/page/{pageId}")
  public String getCollaboratorManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result =
        adminService.getListCollaborator(search, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/collaborator-management/page/1";
    }

    List<Admin> collaborators =
        genericMapper.mapToListOfType((List<?>) result.get("collaborators"), Admin.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(collaborators)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("collaborators", collaborators);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("collaborators", null);
    }
    return "/admin/collaborator-management";
  }

  /**
   * Trang thông tin chi tiết Cộng tác viên
   */
  @GetMapping("/{collaboratorId}/class/page/{pageId}")
  public String getCollaboratorInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "collaboratorId") int collaboratorId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = adminService.getCollaboratorInfo(collaboratorId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/collaborator-management/page/1";
    }

    Admin collaborator = (Admin) result.get("collaborator");

    int size = 5;
    result =
        classService.getListClassByAdminEmail(collaborator.getEmail(), pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/%d/class/page/1", collaboratorId);
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("collaborator", collaborator);
    model.addAttribute("role", RoleEnums.Collaborator.getValue());
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/admin-info";
  }

  // ---------------------------------- USER --------------------------------- //

  /**
   * Trang danh sách Học viên
   */
  @SneakyThrows
  @GetMapping("/user-management/page/{pageId}")
  public String getUserManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = userService.getListUser(search, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/user-management/page/1";
    }

    List<User> users =
        genericMapper.mapToListOfType((List<?>) result.get("users"), User.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(users)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("users", users);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("users", null);
    }
    return "/admin/user-management";
  }

  /**
   * Trang thông tin chi tiết Học viên
   */
  @GetMapping("/user/{userId}/class/page/{pageId}")
  public String getUserInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "userId") int userId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = userService.getUserInfo(userId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/user-management/page/1";
    }

    User user = (User) result.get("user");

    int size = 5;
    result =
        classService.getListClassByUserId(userId, new Search(), pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/user/%d/class/page/1", userId);
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("user", user);
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/user-info";
  }

  /**
   * Trang thêm Học viên
   */
  @GetMapping("/create-user")
  public String getCreateUser(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("user") User user,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (user == null) {
      user = new User();
    }
    model.addAttribute("user", user);
    model.addAttribute("classes", frontendUtils.getAllClass());
    return "/admin/user-create";
  }

  /**
   * Thực thi thêm Học viên
   */
  @SneakyThrows
  @PostMapping("/create-user")
  public String executeCreateUser(
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("user") User user) {
    user.setCreatedBy((String) request.getSession().getAttribute("email"));
    Map<String, Object> result = userService.createUser(user);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("user", user);
      return "redirect:/collaborator/create-user";
    } else {
      return "redirect:/collaborator/user-management/page/1";
    }
  }

  /**
   * Trang cập nhật thông tin Học viên
   */
  @SneakyThrows
  @GetMapping("/update-user/{userId}")
  public String getUpdateUser(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "userId") int userId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = userService.getUserInfo(userId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/user-management/page/1";
    }

    frontendUtils.setLoggedAccountModel(request, model, token);
    User user = (User) result.get("user");
    model.addAttribute("user", user);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/user-update";
  }

  /**
   * Thực thi cập nhật thông tin Học viên
   */
  @SneakyThrows
  @PostMapping("/update-user")
  public String executeUpdateUser(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("user") User user,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = userService.updateUser(user, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/collaborator/update-user/" + user.getId();
    } else {
      return String.format("redirect:/collaborator/user/%d/class/page/1", user.getId());
    }
  }

  // ---------------------------------- TEACHER --------------------------------- //

  /**
   * Trang danh sách Giảng viên
   */
  @SneakyThrows
  @GetMapping("/teacher-management/page/{pageId}")
  public String getTeacherManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = teacherService.getListTeacher(search, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/teacher-management/page/1";
    }

    List<User> teachers =
        genericMapper.mapToListOfType((List<?>) result.get("teachers"), User.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(teachers)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("teachers", teachers);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("teachers", null);
    }
    return "/admin/teacher-management";
  }

  /**
   * Trang thông tin chi tiết Giảng viên
   */
  @GetMapping("/teacher/{teacherId}/class/page/{pageId}")
  public String getTeacherInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "teacherId") int teacherId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = teacherService.getTeacherInfo(teacherId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/teacher-management/page/1";
    }

    User teacher = (User) result.get("teacher");

    int size = 5;
    result =
        classService.getListClassByTeacherId(teacherId, new Search(), pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/user/%d/class/page/1", teacherId);
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    model.addAttribute("teacher", teacher);
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/teacher-info";
  }

  // ---------------------------------- CLASS --------------------------------- //

  /**
   * Trang danh sách các Lớp học
   */
  @SneakyThrows
  @GetMapping("/class-management/page/{pageId}")
  public String getClassManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = classService.getListClass(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/class-management/page/1";
    }

    List<Clazz> classes =
        genericMapper.mapToListOfType((List<?>) result.get("classes"), Clazz.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("listTypeCourse", frontendUtils.getAllTypeCourse());
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(classes)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("classes", classes);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("classes", null);
    }
    return "/admin/class-management";
  }

  /**
   * Trang thông tin chi tiết Lớp học theo danh sách học viên
   */
  @GetMapping("/class/{classId}/student/page/{pageId}")
  public String getDetailClassWithStudents(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = classService.getDetailClass(classId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/class-management/page/1";
    }

    Clazz clazz = (Clazz) result.get("class");

    int size = 5;
    result = registerService.getListRegister(classId, pageId, size, token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/class/%d/student/page/1", classId);
    }

    List<RegisterOutput> registers =
        genericMapper.mapToListOfType((List<?>) result.get("registers"), RegisterOutput.class);
    model.addAttribute("class", clazz);
    model.addAttribute("videoLectures", null);
    model.addAttribute("showRegisters", true);
    model.addAttribute("showVideoLectures", false);
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(registers)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("registers", registers);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("registers", null);
    }
    return "/admin/class-info";
  }

  /**
   * Trang thông tin chi tiết Lớp học theo video bài giảng
   */
  @GetMapping("/class/{classId}/video/page/{pageId}")
  public String getDetailClassWithVideoLectures(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "pageId") int pageId,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = classService.getDetailClass(classId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/class-management/page/1";
    }

    Clazz clazz = (Clazz) result.get("class");

    int size = 5;
    Map<String, Object> resultGetListVideoLecture =
        videoLectureService.getListVideoLecture(classId, new Search(), pageId, size, token);
    int page = (int) resultGetListVideoLecture.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return String.format("redirect:/collaborator/class/%d/video/page/1", classId);
    }

    List<VideoLecture> videoLectures =
        genericMapper.mapToListOfType(
            (List<?>) resultGetListVideoLecture.get("videoLectures"), VideoLecture.class);
    model.addAttribute("class", clazz);
    model.addAttribute("registers", null);
    model.addAttribute("showRegisters", false);
    model.addAttribute("showVideoLectures", true);
    model.addAttribute("notification", standardizedNotification(notification));
    if (!CollectionUtils.isEmpty(videoLectures)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("videoLectures", videoLectures);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("videoLectures", null);
    }
    return "/admin/class-info";
  }

  // ---------------------------------- REGISTER --------------------------------- //

  /**
   * Thực thi thêm học viên vào lớp học
   */
  @GetMapping("/class/{classId}/add-student/{userEmail}")
  public String executeAddStudentIntoClass(
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @PathVariable(name = "userEmail") String userEmail) {
    String adminEmail = (String) request.getSession().getAttribute("email");
    Map<String, Object> result = registerService.adminAddStudent(userEmail, classId, adminEmail);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return String.format("redirect:/collaborator/class/%d/student/page/1/#studentList", classId);
  }

  /**
   * Thực thi cập nhật trạng thái đăng ký học
   */
  @GetMapping("/register/{registerId}/update-state")
  public String executeUpdateStateRegister(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("classId") int classId,
      @ModelAttribute(name = "state") String state,
      @PathVariable(name = "registerId") int registerId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = registerService.updateStateRegister(registerId, state, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return String.format("redirect:/collaborator/class/%d/student/page/1/#studentList", classId);
  }

  /**
   * Thực thi xóa thông tin đăng ký học
   */
  @GetMapping("/delete-register/{registerId}")
  public String executeDeleteRegister(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("classId") int classId,
      @PathVariable(name = "registerId") int registerId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = registerService.deleteRegister(registerId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return String.format("redirect:/collaborator/class/%d/student/page/1/#studentList", classId);
  }

  // ---------------------------------- VIDEO LECTURE --------------------------------- //

  /**
   * Trang thông tin chi tiết bài giảng
   */
  @GetMapping("/video-lecture/{videoLectureId}")
  public String getDetailVideoLecture(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "videoLectureId") int videoLectureId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = videoLectureService.getDetailVideoLecture(videoLectureId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/class-management/page/1";
    } else {
      VideoLecture videoLecture = (VideoLecture) result.get("videoLecture");
      model.addAttribute("videoLecture", videoLecture);
      return "/admin/video-lecture-info";
    }
  }

  // ---------------------------------- TYPE NEWS --------------------------------- //

  /**
   * Trang danh sách loại tin tức
   */
  @SneakyThrows
  @GetMapping("/type-news-management/page/{pageId}")
  public String getTypeNewsManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = typeNewsService.getListTypeNews(search, pageId, size,
        token);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/type-news-management/page/1";
    }

    List<TypeNews> listTypeNews =
        genericMapper.mapToListOfType((List<?>) result.get("listTypeNews"), TypeNews.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(listTypeNews)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("listTypeNews", listTypeNews);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listTypeNews", null);
    }
    return "/admin/type-news-management";
  }

  /**
   * Trang thông tin chi tiết Loại tin tức
   */
  @GetMapping("/type-news/{typeNewsId}")
  public String getDetailTypeNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "typeNewsId") int typeNewsId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = typeNewsService.getDetailTypeNews(typeNewsId, token);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/type-news-management/page/1";
    } else {
      TypeNews typeNews = (TypeNews) result.get("typeNews");
      model.addAttribute("typeNews", typeNews);
      return "/admin/type-news-info";
    }
  }

  // ---------------------------------- NEWS --------------------------------- //

  /**
   * Trang danh sách các Tin tức
   */
  @SneakyThrows
  @GetMapping("/news-management/page/{pageId}")
  public String getNewsManagement(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      @PathVariable(name = "pageId") int pageId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    int size = 5;
    Map<String, Object> result = newsService.getListNews(search, pageId, size);
    int page = (int) result.get("totalPages");
    if ((page == 0 && pageId > 1) || (page != 0 && pageId > page)) {
      return "redirect:/collaborator/news-management/page/1";
    }

    List<News> listNews =
        genericMapper.mapToListOfType((List<?>) result.get("listNews"), News.class);
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("listTypeNews", frontendUtils.getAllTypeNews());
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("search", search);
    if (!CollectionUtils.isEmpty(listNews)) {
      model.addAttribute("page", page);
      model.addAttribute("size", size);
      model.addAttribute("pageId", pageId);
      model.addAttribute("stt", FrontendUtils.getListNumberPage(page));
      model.addAttribute("listNews", listNews);
    } else {
      model.addAttribute("page", 0);
      model.addAttribute("size", 0);
      model.addAttribute("pageId", 0);
      model.addAttribute("stt", 0);
      model.addAttribute("listNews", null);
    }
    return "/admin/news-management";
  }

  /**
   * Trang thông tin chi tiết Tin tức
   */
  @GetMapping("/news/{newsId}")
  public String getDetailNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute Search search,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "newsId") int newsId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, Object> result = newsService.getDetailNews(newsId);
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/news-management/page/1";
    } else {
      News news = (News) result.get("news");
      model.addAttribute("news", news);
      return "/admin/news-info";
    }
  }

  /**
   * Trang thêm Tin tức
   */
  @SneakyThrows
  @GetMapping("/create-news")
  public String getCreateNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("news") News news,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("listTypeNews", frontendUtils.getAllTypeNews());
    if (news == null) {
      news = new News();
    }
    model.addAttribute("news", news);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/news-create";
  }

  /**
   * Thực thi thêm Tin tức
   */
  @SneakyThrows
  @PostMapping("/create-news")
  public String executeCreateNews(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("news") News news,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.createNews(news, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("news", news);
      return "redirect:/manager/create-news";
    } else {
      return "redirect:/collaborator/news-management/page/1";
    }
  }

  /**
   * Trang chỉnh sửa Tin tức
   */
  @SneakyThrows
  @GetMapping("/update-news/{newsId}")
  public String getUpdateNews(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "newsId") int newsId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.getDetailNews(newsId);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/news-management/page/1";
    }
    News news = (News) result.get("news");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("listTypeNews", frontendUtils.getAllTypeNews());
    model.addAttribute("news", news);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/news-update";
  }

  /**
   * Thực thi chỉnh sửa Tin tức
   */
  @SneakyThrows
  @PostMapping("/update-news")
  public String executeUpdateNews(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("news") News news,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.updateNews(news, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-news/" + news.getId();
    } else {
      return "redirect:/collaborator/news/" + news.getId();
    }
  }
}
