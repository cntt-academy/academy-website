package com.example.website_act_frontend.controller;

import static com.example.website_act_frontend.common.utils.FrontendUtils.standardizedNotification;

import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.FrontendUtils;
import com.example.website_act_frontend.model.Admin;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Course;
import com.example.website_act_frontend.model.Slider;
import com.example.website_act_frontend.model.TypeCourse;
import com.example.website_act_frontend.model.TypeNews;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.model.VideoBanner;
import com.example.website_act_frontend.model.VideoLecture;
import com.example.website_act_frontend.service.AdminService;
import com.example.website_act_frontend.service.ClassService;
import com.example.website_act_frontend.service.CourseService;
import com.example.website_act_frontend.service.MediaComponentService;
import com.example.website_act_frontend.service.NewsService;
import com.example.website_act_frontend.service.TeacherService;
import com.example.website_act_frontend.service.TypeCourseService;
import com.example.website_act_frontend.service.TypeNewsService;
import com.example.website_act_frontend.service.UserService;
import com.example.website_act_frontend.service.VideoLectureService;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@RequestMapping("/manager")
@Controller("ManagerController")
public class ManagerController {

  private final MediaComponentService mediaComponentService;
  private final VideoLectureService videoLectureService;
  private final TypeCourseService typeCourseService;
  private final TypeNewsService typeNewsService;
  private final TeacherService teacherService;
  private final FrontendUtils frontendUtils;
  private final CourseService courseService;
  private final ClassService classService;
  private final AdminService adminService;
  private final UserService userService;
  private final NewsService newsService;

  // ---------------------------------- MEDIA COMPONENT --------------------------------- //

  /**
   * Lấy trang thành phần media của website
   */
  @GetMapping("/media-component")
  public String getMediaComponent(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Map<String, String> mediaComponents = frontendUtils.getMediaComponent();

    model.addAttribute("video_banner", mediaComponents.get("video_banner"));
    model.addAttribute("video_update", new VideoBanner());
    model.addAttribute("slider1", mediaComponents.get("slider_01"));
    model.addAttribute("slider2", mediaComponents.get("slider_02"));
    model.addAttribute("slider3", mediaComponents.get("slider_03"));
    model.addAttribute("slider4", mediaComponents.get("slider_04"));
    model.addAttribute("slider5", mediaComponents.get("slider_05"));
    model.addAttribute("slider_update", new Slider());
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/media-component";
  }

  /**
   * Sửa slider
   */
  @PostMapping("/update-slider")
  public String updateSlider(
      @ModelAttribute Slider data,
      RedirectAttributes redirectAttributes,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = mediaComponentService.updateSlider(data, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/manager/media-component";
  }

  /**
   * Sửa video banner
   */
  @PostMapping("/update-video-banner")
  public String updateVideoBanner(
      @ModelAttribute VideoBanner data,
      RedirectAttributes redirectAttributes,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result =
        mediaComponentService.updateVideoBanner(data.getVideoBanner(), token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/manager/media-component";
  }

  // ---------------------------------- MANAGER --------------------------------- //

  /**
   * Trang thông tin Giám đốc trung tâm
   */
  @SneakyThrows
  @GetMapping("/owner-info")
  public String getOwnerInfo(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Admin owner = frontendUtils.getOwnerInfo();
    model.addAttribute("collaborator", owner);
    model.addAttribute("role", RoleEnums.Owner.getValue());
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/admin-info";
  }

  /**
   * Trang chỉnh sửa thông tin Giám đốc trung tâm
   */
  @SneakyThrows
  @GetMapping("/update-owner")
  public String getUpdateOwner(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Admin owner = frontendUtils.getOwnerInfo();
    model.addAttribute("owner", owner);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/owner-update";
  }

  /**
   * Thực thi cập nhật thông tin Giám đốc trung tâm
   */
  @SneakyThrows
  @PostMapping("/update-owner")
  public String executeUpdateOwner(
      @ModelAttribute("owner") Admin owner,
      RedirectAttributes redirectAttributes,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.updateAdmin(owner, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-owner";
    } else {
      return "redirect:/manager/owner-info";
    }
  }

  /**
   * Trang chỉnh sửa thông tin Quản lý trung tâm
   */
  @SneakyThrows
  @GetMapping("/update-manager")
  public String getUpdateManager(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    Admin manager = frontendUtils.getManagerInfo();
    model.addAttribute("manager", manager);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/manager-update";
  }

  /**
   * Thực thi cập nhật thông tin Quản lý trung tâm
   */
  @SneakyThrows
  @PostMapping("/update-manager")
  public String executeUpdateManager(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("manager") Admin manager,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.updateAdmin(manager, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-manager";
    } else {
      return "redirect:/collaborator/manager-info/class/page/1";
    }
  }

  /**
   * Thực thi đổi Quản lý trung tâm
   */
  @GetMapping("/change-manager/{email}")
  public String changeAdmin(
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "email") String email,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.changeManager(email, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/collaborator/manager-info/class/page/1";
    } else {
      if (RoleEnums.Manager.name()
          .equalsIgnoreCase((String) request.getSession().getAttribute("role"))) {
        redirectAttributes.addFlashAttribute(
            "notification",
            "Đổi quản lý trung tâm thành công <br> Xin hãy đăng nhập lại");
        return "redirect:/authentication/logout";
      } else {
        return "redirect:/collaborator/manager-info/class/page/1";
      }
    }
  }

  // ---------------------------------- TYPE COURSE --------------------------------- //

  /**
   * Trang thêm Loại khóa học
   */
  @SneakyThrows
  @GetMapping("/create-type-course")
  public String getCreateTypeCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("typeCourse") TypeCourse typeCourse,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (typeCourse == null) {
      typeCourse = new TypeCourse();
    }
    model.addAttribute("typeCourse", typeCourse);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/type-course-create";
  }

  /**
   * Thực thi thêm Loại khóa học
   */
  @SneakyThrows
  @PostMapping("/create-type-course")
  public String executeCreateTypeCourse(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("typeCourse") TypeCourse typeCourse,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeCourseService.createTypeCourse(typeCourse, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("typeCourse", typeCourse);
      return "redirect:/manager/create-type-course";
    } else {
      return "redirect:/collaborator/type-course-management/page/1";
    }
  }

  /**
   * Trang chỉnh sửa thông tin Loại khóa học
   */
  @SneakyThrows
  @GetMapping("/update-type-course/{typeCourseId}")
  public String getUpdateTypeCourse(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "typeCourseId") int typeCourseId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeCourseService.getDetailTypeCourse(typeCourseId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/type-course-management/page/1";
    }
    TypeCourse typeCourse = (TypeCourse) result.get("typeCourse");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("typeCourse", typeCourse);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/type-course-update";
  }

  /**
   * Thực thi chỉnh sửa thông tin Loại Khóa học
   */
  @SneakyThrows
  @PostMapping("/update-type-course")
  public String executeUpdateTypeCourse(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("typeCourse") TypeCourse typeCourse,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeCourseService.updateTypeCourse(typeCourse, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-type-course/" + typeCourse.getId();
    } else {
      return "redirect:/collaborator/type-course/" + typeCourse.getId();
    }
  }

  /**
   * Thực thi xóa Loại khóa học
   */
  @SneakyThrows
  @GetMapping("/delete-type-course/{typeCourseId}")
  public String deleteTypeCourse(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "typeCourseId") int typeCourseId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeCourseService.deleteTypeCourse(typeCourseId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/type-course-management/page/1";
  }

  // ---------------------------------- COURSE --------------------------------- //

  /**
   * Trang thêm Khóa học
   */
  @SneakyThrows
  @GetMapping("/create-course")
  public String getCreateCourse(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("course") Course course,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("typeCourses", frontendUtils.getAllTypeCourse());
    if (course == null) {
      course = new Course();
    }
    model.addAttribute("course", course);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/course-create";
  }

  /**
   * Thực thi thêm Khóa học
   */
  @SneakyThrows
  @PostMapping("/create-course")
  public String executeCreateCourse(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("course") Course course,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = courseService.createCourse(course, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("course", course);
      return "redirect:/manager/create-course";
    } else {
      return "redirect:/collaborator/course-management/page/1";
    }
  }

  /**
   * Trang chỉnh sửa thông tin Khóa học
   */
  @SneakyThrows
  @GetMapping("/update-course/{courseId}")
  public String getUpdateCourse(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "courseId") int courseId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = courseService.getDetailCourse(courseId);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/course-management/page/1";
    }
    Course course = (Course) result.get("course");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("listTypeCourse", frontendUtils.getAllTypeCourse());
    model.addAttribute("course", course);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/course-update";
  }

  /**
   * Thực thi chỉnh sửa thông tin Khóa học
   */
  @SneakyThrows
  @PostMapping("/update-course")
  public String executeUpdateCourse(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("course") Course course,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = courseService.updateCourse(course, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-course/" + course.getId();
    } else {
      return "redirect:/collaborator/course/" + course.getId();
    }
  }

  /**
   * Thực thi xóa Khóa học
   */
  @SneakyThrows
  @GetMapping("/delete-course/{courseId}")
  public String deleteCourse(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "courseId") int courseId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = courseService.deleteCourse(courseId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/course-management/page/1";
  }

  // ---------------------------------- COLLABORATOR --------------------------------- //

  /**
   * Trang thêm Cộng tác viên
   */
  @GetMapping("/create-collaborator")
  public String getCreateCollaborator(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("collaborator") Admin collaborator,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (collaborator == null) {
      collaborator = new Admin();
    }
    model.addAttribute("collaborator", collaborator);
    return "/admin/collaborator-create";
  }

  /**
   * Thực thi thêm Cộng tác viên
   */
  @SneakyThrows
  @PostMapping("/create-collaborator")
  public String executeCreateCollaborator(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("collaborator") Admin collaborator,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.createCollaborator(collaborator, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("collaborator", collaborator);
      return "redirect:/manager/create-collaborator";
    } else {
      return "redirect:/collaborator/collaborator-management/page/1";
    }
  }

  /**
   * Thực thi xóa Cộng tác viên
   */
  @SneakyThrows
  @GetMapping("/delete-collaborator/{collaboratorId}")
  public String deleteCollaborator(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "collaboratorId") int collaboratorId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = adminService.deleteCollaborator(collaboratorId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/collaborator-management/page/1";
  }

  // ---------------------------------- USER --------------------------------- //

  /**
   * Thực thi xóa Học viên
   */
  @SneakyThrows
  @GetMapping("/delete-user/{userId}")
  public String deleteUser(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "userId") int userId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = userService.deleteUser(userId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/user-management/page/1";
  }

  // ---------------------------------- TEACHER --------------------------------- //

  /**
   * Trang thêm Giảng viên
   */
  @GetMapping("/create-teacher")
  public String getCreateTeacher(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("teacher") User teacher,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (teacher == null) {
      teacher = new User();
    }
    model.addAttribute("teacher", teacher);
    return "/admin/teacher-create";
  }

  /**
   * Thực thi thêm Giảng viên
   */
  @SneakyThrows
  @PostMapping("/create-teacher")
  public String executeCreateTeacher(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("teacher") User teacher,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = teacherService.createTeacher(teacher, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("teacher", teacher);
      return "redirect:/manager/create-teacher";
    } else {
      return "redirect:/collaborator/teacher-management/page/1";
    }
  }

  /**
   * Trang cập nhật thông tin giảng viên
   */
  @SneakyThrows
  @GetMapping("/update-teacher/{teacherId}")
  public String getUpdateTeacher(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "teacherId") int teacherId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = teacherService.getTeacherInfo(teacherId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/teacher-management/page/1";
    }

    frontendUtils.setLoggedAccountModel(request, model, token);
    User teacher = (User) result.get("teacher");
    model.addAttribute("teacher", teacher);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/teacher-update";
  }

  /**
   * Thực thi cập nhật thông tin Giảng viên
   */
  @SneakyThrows
  @PostMapping("/update-teacher")
  public String executeUpdateTeacher(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("teacher") User teacher,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = teacherService.updateTeacher(teacher, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-teacher/" + teacher.getId();
    } else {
      return String.format("redirect:/collaborator/teacher/%d/class/page/1", teacher.getId());
    }
  }

  /**
   * Thực thi xóa Giảng viên
   */
  @SneakyThrows
  @GetMapping("/delete-teacher/{teacherId}")
  public String deleteTeacher(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "teacherId") int teacherId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = teacherService.deleteTeacher(teacherId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/teacher-management/page/1";
  }

  // ---------------------------------- CLASS --------------------------------- //

  /**
   * Trang thêm Lớp học
   */
  @GetMapping("/create-class")
  public String getCreateClass(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("class") Clazz clazz,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (clazz == null) {
      clazz = new Clazz();
    }
    model.addAttribute("teachers", frontendUtils.getAllTeacher());
    model.addAttribute("courses", frontendUtils.getAllCourse());
    model.addAttribute("admins", frontendUtils.getAllAdmin());
    model.addAttribute("class", clazz);
    return "/admin/class-create";
  }

  /**
   * Thực thi thêm Lớp học
   */
  @SneakyThrows
  @PostMapping("/create-class")
  public String executeCreateClass(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("class") Clazz clazz,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = classService.createClass(clazz, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("class", clazz);
      return "redirect:/manager/create-class";
    } else {
      return "redirect:/collaborator/class-management/page/1";
    }
  }

  /**
   * Trang chỉnh sửa thông tin Lớp học
   */
  @SneakyThrows
  @GetMapping("/update-class/{classId}")
  public String getUpdateClass(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = classService.getDetailClass(classId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/class-management/page/1";
    }
    Clazz clazz = (Clazz) result.get("class");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("teachers", frontendUtils.getAllTeacher());
    model.addAttribute("courses", frontendUtils.getAllCourse());
    model.addAttribute("admins", frontendUtils.getAllAdmin());
    model.addAttribute("notification", standardizedNotification(notification));
    model.addAttribute("class", clazz);
    return "/admin/class-update";
  }

  /**
   * Thực thi chỉnh sửa thông tin Lớp học
   */
  @SneakyThrows
  @PostMapping("/update-class")
  public String executeUpdateClass(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("class") Clazz clazz,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = classService.updateClass(clazz, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-class/" + clazz.getId();
    } else {
      return String.format("redirect:/collaborator/class/%d/student/page/1", clazz.getId());
    }
  }

  /**
   * Thực thi xóa Lớp học
   */
  @SneakyThrows
  @GetMapping("/delete-class/{classId}")
  public String deleteClass(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = classService.deleteClass(classId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/class-management/page/1";
  }

  // ---------------------------------- VIDEO LECTURE --------------------------------- //

  /**
   * Trang thêm bài giảng
   */
  @SneakyThrows
  @GetMapping("/class/{classId}/create-video-lecture")
  public String getCreateVideoLecture(
      Model model,
      HttpServletRequest request,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @ModelAttribute("videoLecture") VideoLecture videoLecture,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (videoLecture == null) {
      videoLecture = new VideoLecture();
    }
    videoLecture.setClassId(classId);
    model.addAttribute("videoLecture", videoLecture);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/video-lecture-create";
  }

  /**
   * Thực thi thêm Bài giảng
   */
  @SneakyThrows
  @PostMapping("/create-video-lecture")
  public String executeVideoLecture(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("videoLecture") VideoLecture videoLecture,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = videoLectureService.createVideoLecture(videoLecture, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("videoLecture", videoLecture);
      return String.format("redirect:/manager/class/%d/create-video-lecture",
          videoLecture.getClassId());
    } else {
      return String.format("redirect:/collaborator/class/%d/video/page/1/#videoLectures",
          videoLecture.getClassId());
    }
  }

  /**
   * Trang chỉnh sửa thông tin Bài giảng
   */
  @SneakyThrows
  @GetMapping("/class/{classId}/update-video-lecture/{videoLectureId}")
  public String getUpdateVideoLecture(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "videoLectureId") int videoLectureId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = videoLectureService.getDetailVideoLecture(videoLectureId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return String.format("redirect:/collaborator/class/%d/video/page/1/#videoLectures", classId);
    }
    VideoLecture videoLecture = (VideoLecture) result.get("videoLecture");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("videoLecture", videoLecture);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/video-lecture-update";
  }

  /**
   * Thực thi chỉnh sửa thông tin Khóa học
   */
  @SneakyThrows
  @PostMapping("/update-video-lecture")
  public String executeUpdateVideoLecture(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("videoLecture") VideoLecture videoLecture,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = videoLectureService.updateVideoLecture(videoLecture, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return String.format("redirect:/manager/class/%d/update-video-lecture/%d",
          videoLecture.getClassId(), videoLecture.getId());
    } else {
      return "redirect:/collaborator/video-lecture/" + videoLecture.getId();
    }
  }


  /**
   * Thực thi xóa Bài giảng
   */
  @SneakyThrows
  @GetMapping("/class/{classId}/delete-video-lecture/{videoLectureId}")
  public String deleteVideoLecture(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "classId") int classId,
      @PathVariable(name = "videoLectureId") int videoLectureId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = videoLectureService.deleteVideoLecture(videoLectureId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return String.format("redirect:/collaborator/class/%d/video/page/1/#videoLectures", classId);
  }

  // ---------------------------------- TYPE NEWS --------------------------------- //

  /**
   * Trang thêm Loại tin tức
   */
  @SneakyThrows
  @GetMapping("/create-type-news")
  public String getCreateTypeNews(
      Model model,
      HttpServletRequest request,
      @ModelAttribute("typeNews") TypeNews typeNews,
      @ModelAttribute("notification") String notification,
      @CookieValue(name = "token", defaultValue = "") String token) {
    frontendUtils.setLoggedAccountModel(request, model, token);
    if (typeNews == null) {
      typeNews = new TypeNews();
    }
    model.addAttribute("typeNews", typeNews);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/type-news-create";
  }

  /**
   * Thực thi thêm Loại tin tức
   */
  @SneakyThrows
  @PostMapping("/create-type-news")
  public String executeCreateTypeNews(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("typeNews") TypeNews typeNews,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeNewsService.createTypeNews(typeNews, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      redirectAttributes.addFlashAttribute("typeNews", typeNews);
      return "redirect:/manager/create-type-news";
    } else {
      return "redirect:/collaborator/type-news-management/page/1";
    }
  }

  /**
   * Trang chỉnh sửa thông tin Loại tin tức
   */
  @SneakyThrows
  @GetMapping("/update-type-news/{typeNewsId}")
  public String getUpdateTypeNews(
      Model model,
      HttpServletRequest request,
      RedirectAttributes redirectAttributes,
      @ModelAttribute("notification") String notification,
      @PathVariable(name = "typeNewsId") int typeNewsId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeNewsService.getDetailTypeNews(typeNewsId, token);
    if (result.get("state").equals("fail")) {
      redirectAttributes.addFlashAttribute("notification", result.get("message"));
      return "redirect:/collaborator/type-news-management/page/1";
    }
    TypeNews typeNews = (TypeNews) result.get("typeNews");
    frontendUtils.setLoggedAccountModel(request, model, token);
    model.addAttribute("typeNews", typeNews);
    model.addAttribute("notification", standardizedNotification(notification));
    return "/admin/type-news-update";
  }

  /**
   * Thực thi chỉnh sửa thông tin Loại tin tức
   */
  @SneakyThrows
  @PostMapping("/update-type-news")
  public String executeUpdateTypeNews(
      RedirectAttributes redirectAttributes,
      @ModelAttribute("typeNews") TypeNews typeNews,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeNewsService.updateTypeNews(typeNews, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    if ("fail".equalsIgnoreCase((String) result.get("state"))) {
      return "redirect:/manager/update-type-news/" + typeNews.getId();
    } else {
      return "redirect:/collaborator/type-news/" + typeNews.getId();
    }
  }

  /**
   * Thực thi xóa Loại tin tức
   */
  @SneakyThrows
  @GetMapping("/delete-type-news/{typeNewsId}")
  public String deleteTypeNews(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "typeNewsId") int typeNewsId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = typeNewsService.deleteTypeNews(typeNewsId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/type-news-management/page/1";
  }

  // ---------------------------------- NEWS --------------------------------- //

  /**
   * Thực thi xóa Tin tức
   */
  @SneakyThrows
  @GetMapping("/delete-news/{newsId}")
  public String deleteNews(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "newsId") int newsId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.deleteNews(newsId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/news-management/page/1";
  }

  /**
   * Thực thi duyệt Tin tức
   */
  @SneakyThrows
  @GetMapping("/accept-news/{newsId}")
  public String acceptNews(
      RedirectAttributes redirectAttributes,
      @PathVariable(name = "newsId") int newsId,
      @CookieValue(name = "token", defaultValue = "") String token) {
    Map<String, Object> result = newsService.acceptNews(newsId, token);
    redirectAttributes.addFlashAttribute("notification", result.get("message"));
    return "redirect:/collaborator/news-management/page/1";
  }
}
