package com.example.website_act_frontend.config.interceptor;

import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.JsonObject;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Slf4j
@Component
public class GuestInterceptor implements HandlerInterceptor {

  @Value(value = "${url.authentication.verifyToken}")
  private String urlVerifyToken;
  @Autowired
  private BackendUtils backendUtils;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) {
    // Get token from Cookie
    String token = backendUtils.getToken(request);
    if (StringUtils.isNotBlank(token)) {
      // Set headers
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      // Call API to verify token
      JsonObject resp = UnirestUtils.get(urlVerifyToken, headers);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        request.getSession().setAttribute("email", data.get("email").getAsString());
        request.getSession().setAttribute("name", data.get("name").getAsString());
        request.getSession().setAttribute("role", data.get("role").getAsString());
      }
    }
    return true;
  }
}
