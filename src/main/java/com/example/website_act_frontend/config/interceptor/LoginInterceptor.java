package com.example.website_act_frontend.config.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(
      HttpServletRequest request, HttpServletResponse response, Object handler) {
    Object resetLogin = request.getSession().getAttribute("resetLogin");
    if (!ObjectUtils.isEmpty(resetLogin) && (Boolean) resetLogin) {
      Cookie[] cookies = request.getCookies();
      if (cookies != null) {
        for (Cookie cookie : cookies) {
          cookie.setMaxAge(0);
          cookie.setPath("/");
          cookie.setHttpOnly(true);
          cookie.setSecure(true);
          response.addCookie(cookie);
        }
      }
    }
    return true;
  }
}
