package com.example.website_act_frontend.config;

import org.apache.commons.codec.CharEncoding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@Configuration
public class ThymeleafConfiguration {

  @Bean
  public ClassLoaderTemplateResolver classLoaderTemplateResolver() {
    ClassLoaderTemplateResolver classLoaderTemplateResolver = new ClassLoaderTemplateResolver();
    classLoaderTemplateResolver.setPrefix("templates/");
    classLoaderTemplateResolver.setSuffix(".html");
    classLoaderTemplateResolver.setTemplateMode("HTML5");
    classLoaderTemplateResolver.setCharacterEncoding(CharEncoding.UTF_8);
    classLoaderTemplateResolver.setOrder(1);
    return classLoaderTemplateResolver;
  }
}
