package com.example.website_act_frontend.config;

import com.example.website_act_frontend.config.interceptor.CollaboratorInterceptor;
import com.example.website_act_frontend.config.interceptor.GuestInterceptor;
import com.example.website_act_frontend.config.interceptor.LoginInterceptor;
import com.example.website_act_frontend.config.interceptor.ManagerInterceptor;
import com.example.website_act_frontend.config.interceptor.UserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class MvcConfig implements WebMvcConfigurer {

  @Autowired
  private CollaboratorInterceptor collaboratorInterceptor;
  @Autowired
  private ManagerInterceptor managerInterceptor;
  @Autowired
  private GuestInterceptor guestInterceptor;
  @Autowired
  private UserInterceptor userInterceptor;
  @Autowired
  private LoginInterceptor loginInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry
        .addInterceptor(loginInterceptor)
        .addPathPatterns("/authentication/login")
        .excludePathPatterns(
            "/authentication/forgetPassword", "/authentication/doLogin", "/authentication/logout",
            "/user/**", "/collaborator/**", "/manager/**");
    registry
        .addInterceptor(guestInterceptor)
        .addPathPatterns("/**")
        .excludePathPatterns("/authentication/**", "/user/**", "/collaborator/**", "/manager/**");
    registry
        .addInterceptor(userInterceptor)
        .addPathPatterns("/user/**")
        .excludePathPatterns("/authentication/**", "/collaborator/**", "/manager/**");
    registry
        .addInterceptor(collaboratorInterceptor)
        .addPathPatterns("/collaborator/**")
        .excludePathPatterns("/authentication/**", "/user/**", "/manager/**");
    registry
        .addInterceptor(managerInterceptor)
        .addPathPatterns("/manager/**")
        .excludePathPatterns("/authentication/**", "/user/**", "/collaborator/**");
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    registry.addResourceHandler("/script/**").addResourceLocations("classpath:/script/");
    registry.addResourceHandler("/video/**").addResourceLocations("classpath:/video/");
  }
}
