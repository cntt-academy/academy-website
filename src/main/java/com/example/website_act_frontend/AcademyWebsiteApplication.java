package com.example.website_act_frontend;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Slf4j
@SpringBootApplication
public class AcademyWebsiteApplication {

  @Value("${cloudinary.api_secret}")
  private String apiSecret;
  @Value("${cloudinary.api_key}")
  private String apiKey;
  @Value("${cloudinary.cloud_name}")
  private String cloudName;

  @Value(value = "${url.media-component.getMediaComponent}")
  private String urlGetMediaComponent;
  @Value(value = "${url.admin.getManagerInfo}")
  private String urlGetManagerInfo;
  @Value(value = "${url.type-course.getAll}")
  private String urlGetAllTypeCourse;
  @Value(value = "${url.type-news.getAll}")
  private String urlGetAllTypeNews;
  @Value(value = "${url.teacher.getAll}")
  private String urlGetAllTeacher;
  @Value(value = "${url.course.getAll}")
  private String urlGetAllCourse;
  @Value(value = "${url.admin.getOwnerInfo}")
  private String urlGetOwnerInfo;
  @Value(value = "${url.admin.getAll}")
  private String urlGetAllAdmin;
  @Value(value = "${url.class.getAll}")
  private String urlGetAllClass;

  public static void main(String[] args) {
    SpringApplication.run(AcademyWebsiteApplication.class, args);
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    return modelMapper;
  }

  @Bean
  public CommonsMultipartResolver multipartResolver() {
    CommonsMultipartResolver resolver = new CommonsMultipartResolver();
    resolver.setDefaultEncoding("UTF-8");
    return resolver;
  }

  @Bean
  public Cloudinary cloudinary() {
    return new Cloudinary(ObjectUtils.asMap(
        "cloud_name", cloudName,
        "api_key", apiKey,
        "api_secret", apiSecret,
        "secure", true));
  }

  @Bean
  public void setUrlSetRedis() {
    log.info("-------------- PRE HANDLER - CALL LIST API SET REDIS --------------");
    JsonObject resp;
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetMediaComponent);
    log.info("<Result> => Call API get media component status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTypeCourse);
    log.info("<Result> => Call API get all type course status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTypeNews);
    log.info("<Result> => Call API get all type news status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetManagerInfo);
    log.info("<Result> => Call API get manager info status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTeacher);
    log.info("<Result> => Call API get all teacher status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllCourse);
    log.info("<Result> => Call API get all course status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetOwnerInfo);
    log.info("<Result> => Call API get owner info status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllAdmin);
    log.info("<Result> => Call API get all admin status: {}", resp.get("status"));
    resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllClass);
    log.info("<Result> => Call API get all class status: {}", resp.get("status"));
    log.info("---------------------------------------------------------------------");
  }
}
