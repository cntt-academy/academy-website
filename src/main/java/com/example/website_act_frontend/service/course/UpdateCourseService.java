package com.example.website_act_frontend.service.course;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Course;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateCourseService")
public class UpdateCourseService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.course.update}")
  private String urlUpdateCourse;

  public Map<String, Object> execute(Course course, String token) {
    log.info("======== COURSE - UPDATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      if (course.getFile() != null && !course.getFile().isEmpty()) {
        Transformation<?> incoming =
            new Transformation<>().crop("fill").width(160).height(150);
        String courseImg =
            cloudinaryUtils.uploadFile(course.getFile(), "course", "course", incoming);
        if (StringUtils.isNotBlank(courseImg)) {
          course.setImg(courseImg);
        }
      }

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(course);
      JsonObject resp = UnirestUtils.put(urlUpdateCourse, headers, body);
      log.info("Url: {}", urlUpdateCourse);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating course result: {}", gson.toJson(result));
    return result;
  }
}
