package com.example.website_act_frontend.service.course;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Course;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailCourseService")
public class GetDetailCourseService {

  private final Gson gson;

  @Value(value = "${url.course.getDetail}")
  private String urlGetDetailCourse;

  public Map<String, Object> execute(int courseId) {
    log.info("======== COURSE - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, Object> parameters = Map.of("courseId", courseId);
      JsonObject resp = UnirestUtils.getWithoutHeader(urlGetDetailCourse, parameters);
      log.info("Url: {}", urlGetDetailCourse);
      log.info("Course Id: {}", courseId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        Course course = gson.fromJson(data.get("course"), Course.class);
        result.put("state", "success");
        result.put("course", course);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail course result: {}", gson.toJson(result));
    return result;
  }
}
