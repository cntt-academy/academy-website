package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Course;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.service.course.CreateCourseService;
import com.example.website_act_frontend.service.course.DeleteCourseService;
import com.example.website_act_frontend.service.course.GetDetailCourseService;
import com.example.website_act_frontend.service.course.GetListCourseService;
import com.example.website_act_frontend.service.course.UpdateCourseService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("CourseService")
public class CourseService {
  private final GetDetailCourseService getDetailCourseService;
  private final GetListCourseService getListCourseService;
  private final CreateCourseService createCourseService;
  private final UpdateCourseService updateCourseService;
  private final DeleteCourseService deleteCourseService;

  /**
   * Gọi API để lấy ra danh sách khóa học
   */
  public Map<String, Object> getListCourse(Search search, int page, int size) {
    return getListCourseService.execute(search, page, size);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết khóa học
   */
  public Map<String, Object> getDetailCourse(int courseId) {
    return getDetailCourseService.execute(courseId);
  }

  /**
   * Gọi API để tạo mới khóa học
   */
  public Map<String, Object> createCourse(Course course, String token) {
    return createCourseService.execute(course, token);
  }

  /**
   * Gọi API để cập nhật khóa học
   */
  public Map<String, Object> updateCourse(Course course, String token) {
    return updateCourseService.execute(course, token);
  }

  /**
   * Gọi API để xóa khóa học
   */
  public Map<String, Object> deleteCourse(int courseId, String token) {
    return deleteCourseService.execute(courseId, token);
  }
}
