package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Admin;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.service.admin.ChangeManagerService;
import com.example.website_act_frontend.service.admin.CreateCollaboratorService;
import com.example.website_act_frontend.service.admin.DeleteCollaboratorService;
import com.example.website_act_frontend.service.admin.GetCollaboratorInfoService;
import com.example.website_act_frontend.service.admin.GetListCollaboratorService;
import com.example.website_act_frontend.service.admin.UpdateAdminService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("AdminService")
public class AdminService {

  private final GetListCollaboratorService getListCollaboratorService;
  private final GetCollaboratorInfoService getCollaboratorInfoService;
  private final CreateCollaboratorService createCollaboratorService;
  private final DeleteCollaboratorService deleteCollaboratorService;
  private final ChangeManagerService changeManagerService;
  private final UpdateAdminService updateAdminService;

  /**
   * Gọi API để lấy ra danh sách Cộng tác viên
   */
  public Map<String, Object> getListCollaborator(
      Search search, int page, int size, String token) {
    return getListCollaboratorService.execute(search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết Cộng tác viên
   */
  public Map<String, Object> getCollaboratorInfo(int collaboratorId, String token) {
    return getCollaboratorInfoService.execute(collaboratorId, token);
  }

  /**
   * Gọi API để thêm mới Cộng tác viên
   */
  public Map<String, Object> createCollaborator(Admin collaborator, String token) {
    return createCollaboratorService.execute(collaborator, token);
  }

  /**
   * Gọi API để cập nhật thông tin Admin
   */
  public Map<String, Object> updateAdmin(Admin admin, String token) {
    return updateAdminService.execute(admin, token);
  }

  /**
   * Gọi API để xóa Cộng tác viên
   */
  public Map<String, Object> deleteCollaborator(int collaboratorId, String token) {
    return deleteCollaboratorService.execute(collaboratorId, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết Cộng tác viên
   */
  public Map<String, Object> changeManager(String email, String token) {
    return changeManagerService.execute(email, token);
  }
}