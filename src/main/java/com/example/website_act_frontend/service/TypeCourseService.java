package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.TypeCourse;
import com.example.website_act_frontend.service.typeCourse.CreateTypeCourseService;
import com.example.website_act_frontend.service.typeCourse.DeleteTypeCourseService;
import com.example.website_act_frontend.service.typeCourse.GetDetailTypeCourseService;
import com.example.website_act_frontend.service.typeCourse.GetListTypeCourseService;
import com.example.website_act_frontend.service.typeCourse.UpdateTypeCourseService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("TypeCourseService")
public class TypeCourseService {

  private final GetDetailTypeCourseService getDetailTypeCourseService;
  private final GetListTypeCourseService getListTypeCourseService;
  private final CreateTypeCourseService createTypeCourseService;
  private final UpdateTypeCourseService updateTypeCourseService;
  private final DeleteTypeCourseService deleteTypeCourseService;

  /**
   * Gọi API để lấy ra danh sách loại khóa học
   */
  public Map<String, Object> getListTypeCourse(Search search, int page, int size, String token) {
    return getListTypeCourseService.execute(search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết loại khóa học
   */
  public Map<String, Object> getDetailTypeCourse(int typeCourseId, String token) {
    return getDetailTypeCourseService.execute(typeCourseId, token);
  }

  /**
   * Gọi API để tạo mới loại khóa học
   */
  public Map<String, Object> createTypeCourse(TypeCourse typeCourse, String token) {
    return createTypeCourseService.execute(typeCourse, token);
  }

  /**
   * Gọi API để cập nhật loại khóa học
   */
  public Map<String, Object> updateTypeCourse(TypeCourse typeCourse, String token) {
    return updateTypeCourseService.execute(typeCourse, token);
  }

  /**
   * Gọi API để xóa loại khóa học
   */
  public Map<String, Object> deleteTypeCourse(int typeCourseId, String token) {
    return deleteTypeCourseService.execute(typeCourseId, token);
  }
}
