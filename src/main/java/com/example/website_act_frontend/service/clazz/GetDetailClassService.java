package com.example.website_act_frontend.service.clazz;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Clazz;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailClassService")
public class GetDetailClassService {

  private final Gson gson;

  @Value(value = "${url.class.getDetail}")
  private String urlGetDetailClass;

  public Map<String, Object> execute(int classId, String token) {
    log.info("======== CLASS - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("classId", classId);
      JsonObject resp = UnirestUtils.get(urlGetDetailClass, headers, parameters);
      log.info("Url: {}", urlGetDetailClass);
      log.info("Class Id: {}", classId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        Clazz clazz = gson.fromJson(data.get("class"), Clazz.class);
        if (StringUtils.isNotBlank(clazz.getDateOpening())) {
          clazz.setDateOpeningLocalDate(
              LocalDate.parse(clazz.getDateOpening(), BackendUtils.formatter));
        }
        result.put("state", "success");
        result.put("class", clazz);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail class result: {}", gson.toJson(result));
    return result;
  }
}
