package com.example.website_act_frontend.service.clazz;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Clazz;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateClassService")
public class UpdateClassService {

  private final Gson gson;
  @Value(value = "${url.class.update}")
  private String urlUpdateClass;

  public Map<String, Object> execute(Clazz clazz, String token) {
    log.info("======== CLASS - UPDATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      clazz.setDateOpening(BackendUtils.formatter.format(clazz.getDateOpeningLocalDate()));

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(clazz);
      JsonObject resp = UnirestUtils.put(urlUpdateClass, headers, body);
      log.info("Url: {}", urlUpdateClass);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating class result: {}", gson.toJson(result));
    return result;
  }
}
