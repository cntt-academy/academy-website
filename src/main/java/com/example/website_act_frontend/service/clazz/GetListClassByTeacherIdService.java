package com.example.website_act_frontend.service.clazz;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.PageRequest;
import com.example.website_act_frontend.model.Search;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("GetListClassByTeacherIdService")
public class GetListClassByTeacherIdService {

  private final GenericMapper genericMapper;
  private final Gson gson;

  @Value(value = "${url.class.getList}")
  private String urlGetListClass;

  public Map<String, Object> execute(Integer teacherId, Search search, int page, int size,
      String token) {
    log.info("======== CLASS - GET LIST BY TEACHER ID ========");
    Map<String, Object> result = new HashMap<>();
    result.put("classes", new ArrayList<>());
    result.put("totalElements", 0);
    result.put("totalPages", 0);

    try {
      Map<String, Object> body = new HashMap<>();
      body.put("stateNot", "PREP");
      body.put("teacherId", teacherId);
      body.put("state", search.getState());
      body.put("search", search.getContent());
      body.put("pageRequest", new PageRequest(page, size, "createdDate", true));

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      JsonObject resp = UnirestUtils.postByBody(urlGetListClass, headers, gson.toJson(body));
      log.info("Url: {}", urlGetListClass);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
      } else {
        JsonObject data = resp.get("data").getAsJsonObject();
        List<?> classes = gson.fromJson(data.get("classes"), List.class);
        if (!CollectionUtils.isEmpty(result)) {
          result.replace("classes", genericMapper.mapToListOfType(classes, Clazz.class));
          result.replace("totalElements", data.get("totalElements").getAsInt());
          result.replace("totalPages", data.get("totalPages").getAsInt());
        }
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting list class result: {}", gson.toJson(result));
    return result;
  }
}
