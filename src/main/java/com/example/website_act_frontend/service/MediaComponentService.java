package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Slider;
import com.example.website_act_frontend.service.mediaComponent.UpdateSliderService;
import com.example.website_act_frontend.service.mediaComponent.UpdateVideoBannerService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@Service("MediaComponentService")
public class MediaComponentService {

  private final UpdateVideoBannerService updateVideoBannerService;
  private final UpdateSliderService updateSliderService;

  /**
   * Gọi API để update video banner
   */
  public Map<String, Object> updateVideoBanner(MultipartFile videoBanner, String token) {
    return updateVideoBannerService.execute(videoBanner, token);
  }

  /**
   * Gọi API để update slider
   */
  public Map<String, Object> updateSlider(Slider slider, String token) {
    return updateSliderService.execute(slider, token);
  }
}
