package com.example.website_act_frontend.service.videoLecture;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.VideoLecture;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailVideoLectureService")
public class GetDetailVideoLectureService {

  private final Gson gson;

  @Value(value = "${url.video-lecture.getDetail}")
  private String urlGetDetailVideoLecture;

  public Map<String, Object> execute(int videoLectureId, String token) {
    log.info("======== VIDEO LECTURE - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("videoLectureId", videoLectureId);
      JsonObject resp = UnirestUtils.get(urlGetDetailVideoLecture, headers, parameters);
      log.info("Url: {}", urlGetDetailVideoLecture);
      log.info("Video lecture Id: {}", videoLectureId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        VideoLecture videoLecture = gson.fromJson(data.get("videoLecture"), VideoLecture.class);
        result.put("state", "success");
        result.put("videoLecture", videoLecture);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail video lecture result: {}", gson.toJson(result));
    return result;
  }
}
