package com.example.website_act_frontend.service.videoLecture;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.VideoLecture;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateVideoLectureService")
public class UpdateVideoLectureService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.video-lecture.update}")
  private String urlUpdateVideoLecture;

  public Map<String, Object> execute(VideoLecture videoLecture, String token) {
    log.info("======== VIDEO LECTURE - UPDATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      if (videoLecture.getFile() != null && !videoLecture.getFile().isEmpty()) {
        String videoName = "class/" + videoLecture.getClassId() + "/" +System.currentTimeMillis() + ".mp4";
        videoLecture.setUrl(
            cloudinaryUtils.uploadFile(videoLecture.getFile(), "video-lecture", videoName, null));
      }
      String body = gson.toJson(videoLecture);

      JsonObject resp = UnirestUtils.put(urlUpdateVideoLecture, headers, body);
      log.info("Url: {}", urlUpdateVideoLecture);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
        result.put("message", "Cập nhật bài giảng thành công");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating video lecture result: {}", gson.toJson(result));
    return result;
  }

  @SneakyThrows
  private String uploadFileToFolderVideo(MultipartFile file, Integer classId) {
    String pth = System.getProperty("user.dir");
    String strFolderExport = new File(pth).getParent() + "/video/class_" + classId + "/";
    File folderExport = new File(strFolderExport);
    if (!folderExport.exists()) {
      folderExport.mkdirs();
    }
    String fileName = System.currentTimeMillis() + ".mp4";
    String filePath = strFolderExport + fileName;
    OutputStream os = new FileOutputStream(filePath.replace('\\', '/'));
    os.write(file.getBytes());
    os.close();

    ResourceUtils.getFile(fileName);
    return filePath;
  }
}
