package com.example.website_act_frontend.service.videoLecture;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteVideoLectureService")
public class DeleteVideoLectureService {

  private final Gson gson;

  @Value(value = "${url.video-lecture.delete}")
  private String urlDeleteVideoLecture;

  public Map<String, Object> execute(Integer videoLectureId, String token) {
    log.info("======== VIDEO LECTURE - DELETE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("videoLectureId", videoLectureId);
      JsonObject resp = UnirestUtils.delete(urlDeleteVideoLecture, headers, parameters);
      log.info("Url: {}", urlDeleteVideoLecture);
      log.info("Video lecture Id: {}", videoLectureId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Deleting video lecture result: {}", gson.toJson(result));
    return result;
  }
}
