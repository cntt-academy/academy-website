package com.example.website_act_frontend.service.admin;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Admin;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetCollaboratorInfoService")
public class GetCollaboratorInfoService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.admin.getById}")
  private String urlGetById;

  public Map<String, Object> execute(int collaboratorId, String token) {
    log.info("======== ADMIN - GET BY ID ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters =
          Map.of("adminId", aesUtils.encrypt(String.valueOf(collaboratorId)));
      JsonObject resp = UnirestUtils.get(urlGetById, headers, parameters);
      log.info("Url: {}", urlGetById);
      log.info("Admin Id: {}", collaboratorId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        Admin collaborator = gson.fromJson(data.get("admin"), Admin.class);
        collaborator.setDobLocalDate(
            LocalDate.parse(collaborator.getDob(), BackendUtils.formatter));
        result.put("state", "success");
        result.put("collaborator", collaborator);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting collaborator info result: {}", gson.toJson(result));
    return result;
  }
}
