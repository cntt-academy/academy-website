package com.example.website_act_frontend.service.admin;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Admin;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateAdminService")
public class UpdateAdminService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.admin.update}")
  private String urlUpdateAdmin;

  public Map<String, Object> execute(Admin admin, String token) {
    log.info("======== ADMIN - UPDATE ADMIN ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Set ảnh đại diện
      if (admin.getFile() != null && !admin.getFile().isEmpty()) {
        Transformation<?> incoming =
            new Transformation<>()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        String[] email = admin.getEmail().split("@");
        String avatar =
            cloudinaryUtils.uploadFile(admin.getFile(), "admin", email[0], incoming);
        if (StringUtils.isNotBlank(avatar)) {
          admin.setAvatar(avatar);
        }
      }

      admin.setDob(BackendUtils.formatter.format(admin.getDobLocalDate()));

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(admin);
      JsonObject resp = UnirestUtils.put(urlUpdateAdmin, headers, body);
      log.info("Url: {}", urlUpdateAdmin);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
        if (admin.getRole().equals(RoleEnums.Owner.name())) {
          result.put("message", "Cập nhật thông tin Giám đốc trung tâm thành công");
        } else if (admin.getRole().equals(RoleEnums.Manager.name())) {
          result.put("message", "Cập nhật thông tin Quản lý trung tâm thành công");
        } else {
          result.put("message", "Cập nhật thông tin Cộng tác viên thành công");
        }
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating admin result: {}", gson.toJson(result));
    return result;
  }
}
