package com.example.website_act_frontend.service.admin;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.constant.ImageConstant;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Admin;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateCollaboratorService")
public class CreateCollaboratorService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.admin.create}")
  private String urlCreateCollaborator;

  public Map<String, Object> execute(Admin collaborator, String token) {
    log.info("======== ADMIN - CREATE COLLABORATOR ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Set ảnh đại diện
      String avatar;
      if (collaborator.getFile() == null || collaborator.getFile().isEmpty()) {
        avatar = ImageConstant.unknownUserImage;
      } else {
        Transformation<?> incoming =
            new Transformation<>()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        String[] email = collaborator.getEmail().split("@");
        avatar =
            cloudinaryUtils.uploadFile(collaborator.getFile(), "admin", email[0], incoming);
        if (StringUtils.isBlank(avatar)) {
          avatar = ImageConstant.unknownUserImage;
        }
      }
      collaborator.setAvatar(avatar);
      collaborator.setDob(BackendUtils.convertStrDate(collaborator.getDob()));

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(collaborator);
      JsonObject resp =
          UnirestUtils.postByBody(urlCreateCollaborator, headers, body);
      collaborator.setPassword(null);
      log.info("Url: {}", urlCreateCollaborator);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating collaborator result: {}", gson.toJson(result));
    return result;
  }
}
