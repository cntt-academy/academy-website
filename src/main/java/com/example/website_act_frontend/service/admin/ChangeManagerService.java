package com.example.website_act_frontend.service.admin;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangeManagerService")
public class ChangeManagerService {

  private final Gson gson;

  @Value(value = "${url.admin.changeManager}")
  private String urlChangeManager;

  public Map<String, Object> execute(String email, String token) {
    log.info("======== ADMIN - CHANGE MANAGER ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> body = Map.of("email", email);
      JsonObject resp = UnirestUtils.put(urlChangeManager, headers, gson.toJson(body));
      log.info("Url: {}", urlChangeManager);
      log.info("Email of new manager: {}", email);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Changing manager result: {}", gson.toJson(result));
    return result;
  }
}
