package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.VideoLecture;
import com.example.website_act_frontend.service.videoLecture.AddHistoryWatchService;
import com.example.website_act_frontend.service.videoLecture.CreateVideoLectureService;
import com.example.website_act_frontend.service.videoLecture.DeleteVideoLectureService;
import com.example.website_act_frontend.service.videoLecture.GetDetailVideoLectureService;
import com.example.website_act_frontend.service.videoLecture.GetListVideoLectureService;
import com.example.website_act_frontend.service.videoLecture.UpdateVideoLectureService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("VideoLectureService")
public class VideoLectureService {

  private final GetDetailVideoLectureService getDetailVideoLectureService;
  private final GetListVideoLectureService getListVideoLectureService;
  private final CreateVideoLectureService createVideoLectureService;
  private final UpdateVideoLectureService updateVideoLectureService;
  private final DeleteVideoLectureService deleteVideoLectureService;
  private final AddHistoryWatchService addHistoryWatchService;

  /**
   * Gọi API để lấy ra danh sách video bài giảng
   */
  public Map<String, Object> getListVideoLecture(
      Integer classId, Search search, int page, int size, String token) {
    return getListVideoLectureService.execute(classId, search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết video bài giảng
   */
  public Map<String, Object> getDetailVideoLecture(Integer videoLectureId, String token) {
    return getDetailVideoLectureService.execute(videoLectureId, token);
  }

  /**
   * Gọi API để tạo video bài giảng
   */
  public Map<String, Object> createVideoLecture(VideoLecture videoLecture, String token) {
    return createVideoLectureService.execute(videoLecture, token);
  }

  /**
   * Gọi API để cập nhật video bài giảng
   */
  public Map<String, Object> updateVideoLecture(VideoLecture videoLecture, String token) {
    return updateVideoLectureService.execute(videoLecture, token);
  }

  /**
   * Gọi API để xóa video bài giảng
   */
  public Map<String, Object> deleteVideoLecture(Integer videoLectureId, String token) {
    return deleteVideoLectureService.execute(videoLectureId, token);
  }

  /**
   * Gọi API để xóa video bài giảng
   */
  public void addHistoryWatch(Integer videoLectureId, String token) {
    addHistoryWatchService.execute(videoLectureId, token);
  }
}
