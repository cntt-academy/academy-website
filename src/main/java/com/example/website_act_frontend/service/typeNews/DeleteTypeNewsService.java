package com.example.website_act_frontend.service.typeNews;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteTypeNewsService")
public class DeleteTypeNewsService {

  private final Gson gson;

  @Value(value = "${url.type-news.delete}")
  private String urlDeleteTypeNews;

  public Map<String, Object> execute(int typeNewsId, String token) {
    log.info("======== TYPE NEWS - DELETE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("typeNewsId", typeNewsId);
      JsonObject resp = UnirestUtils.delete(urlDeleteTypeNews, headers, parameters);
      log.info("Url: {}", urlDeleteTypeNews);
      log.info("Type news Id: {}", typeNewsId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Deleting type news result: {}", gson.toJson(result));
    return result;
  }
}
