package com.example.website_act_frontend.service.typeNews;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.TypeNews;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTypeNewsService")
public class CreateTypeNewsService {

  private final Gson gson;
  @Value(value = "${url.type-news.create}")
  private String urlCreateTypeNews;

  public Map<String, Object> execute(TypeNews typeNews, String token) {
    log.info("======== TYPE NEWS - CREATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(typeNews);
      JsonObject resp = UnirestUtils.postByBody(urlCreateTypeNews, headers, body);
      log.info("Url: {}", urlCreateTypeNews);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating type news result: {}", gson.toJson(result));
    return result;
  }
}
