package com.example.website_act_frontend.service.typeNews;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.TypeNews;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailTypeNewsService")
public class GetDetailTypeNewsService {

  private final Gson gson;

  @Value(value = "${url.type-news.getDetail}")
  private String urlGetDetailTypeNews;

  public Map<String, Object> execute(int typeNewsId, String token) {
    log.info("======== TYPE NEWS - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> parameters = Map.of("typeNewsId", typeNewsId);
      JsonObject resp = UnirestUtils.get(urlGetDetailTypeNews, headers, parameters);
      log.info("Url: {}", urlGetDetailTypeNews);
      log.info("Type news Id: {}", typeNewsId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        TypeNews typeNews = gson.fromJson(data.get("typeNews"), TypeNews.class);
        result.put("state", "success");
        result.put("typeNews", typeNews);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail type news result: {}", gson.toJson(result));
    return result;
  }
}
