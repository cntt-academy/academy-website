package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.service.user.CreateUserService;
import com.example.website_act_frontend.service.user.DeleteUserService;
import com.example.website_act_frontend.service.user.GetListTopUserService;
import com.example.website_act_frontend.service.user.GetListUserService;
import com.example.website_act_frontend.service.user.GetUserInfoService;
import com.example.website_act_frontend.service.user.UpdateUserService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("UserService")
public class UserService {

  private final GetListTopUserService getListTopUserService;
  private final GetListUserService getListUserService;
  private final GetUserInfoService getUserInfoService;
  private final CreateUserService createUserService;
  private final UpdateUserService updateUserService;
  private final DeleteUserService deleteUserService;

  /**
   * Gọi API để lấy ra danh sách top Học viên
   */
  public Map<String, Object> getListTopUser(int page, int size) {
    return getListTopUserService.execute(page, size);
  }

  /**
   * Gọi API để lấy ra danh sách Học viên
   */
  public Map<String, Object> getListUser(
      Search search, int page, int size, String token) {
    return getListUserService.execute(search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết Học viên
   */
  public Map<String, Object> getUserInfo(int userId, String token) {
    return getUserInfoService.execute(userId, token);
  }

  /**
   * Gọi API để thêm Học viên
   */
  public Map<String, Object> createUser(User user) {
    return createUserService.execute(user);
  }

  /**
   * Gọi API để cập nhật thông tin Học viên
   */
  public Map<String, Object> updateUser(User user, String token) {
    return updateUserService.execute(user, token);
  }

  /**
   * Gọi API để xóa Học viên
   */
  public Map<String, Object> deleteUser(Integer userId, String token) {
    return deleteUserService.execute(userId, token);
  }
}
