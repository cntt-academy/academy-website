package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.ChangePassword;
import com.example.website_act_frontend.model.Login;
import com.example.website_act_frontend.service.authentication.ChangePasswordService;
import com.example.website_act_frontend.service.authentication.ForgetPasswordService;
import com.example.website_act_frontend.service.authentication.LoginService;
import com.example.website_act_frontend.service.authentication.LogoutService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("AuthenticationService")
public class AuthenticationService {

  private final ForgetPasswordService forgetPasswordService;
  private final ChangePasswordService changePasswordService;
  private final LogoutService logoutService;
  private final LoginService loginService;

  /**
   * Gọi API để Login
   */
  public Map<String, Object> login(Login login) {
    return loginService.execute(login);
  }

  /**
   * Gọi API để cấp mật khẩu mới cho tài khoản
   */
  public Map<String, Object> forgetPassword(String email) {
    return forgetPasswordService.execute(email);
  }

  /**
   * Gọi API để đổi mật khẩu
   */
  public Map<String, Object> changePassword(ChangePassword request, String token) {
    return changePasswordService.execute(request, token);
  }

  /**
   * Gọi API để Logout
   */
  public Map<String, Object> logout(String token) {
    return logoutService.execute(token);
  }

}
