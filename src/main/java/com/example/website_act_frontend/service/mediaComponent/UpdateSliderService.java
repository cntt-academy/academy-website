package com.example.website_act_frontend.service.mediaComponent;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Slider;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateSliderService")
public class UpdateSliderService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.media-component.updateSlider}")
  private String urlUpdateSlider;

  public Map<String, Object> execute(Slider slider, String token) {
    log.info("======== MEDIA COMPONENT - UPDATE SLIDER ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> urlMap = new HashMap<>();

      MultipartFile[] sliders = {
          slider.getSlider1(),
          slider.getSlider2(),
          slider.getSlider3(),
          slider.getSlider4(),
          slider.getSlider5()
      };

      for (int i = 0; i < sliders.length; i++) {
        MultipartFile sliderUrl = sliders[i];
        if (sliderUrl != null && !sliderUrl.isEmpty()) {
          String name = "slider_" + String.format("%02d", i + 1) + "_" + System.currentTimeMillis();
          String urlKey = "urlSlider" + (i + 1);
          urlMap.put(urlKey, cloudinaryUtils.uploadImageSlider(sliderUrl, name));
        }
      }

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(urlMap);
      JsonObject resp = UnirestUtils.put(urlUpdateSlider, headers, body);
      log.info("Url: {}", urlUpdateSlider);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating slider result: {}", gson.toJson(result));
    return result;
  }


}
