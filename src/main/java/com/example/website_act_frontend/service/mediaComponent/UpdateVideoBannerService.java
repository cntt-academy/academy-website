package com.example.website_act_frontend.service.mediaComponent;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateVideoBannerService")
public class UpdateVideoBannerService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.media-component.updateVideoBanner}")
  private String urlUpdateVideoBanner;

  public Map<String, Object> execute(MultipartFile videoBanner, String token) {
    log.info("======== MEDIA COMPONENT - UPDATE VIDEO BANNER ========");
    Map<String, Object> result = new HashMap<>();

    try {
      String urlVideoBanner = "";
      if (videoBanner != null && !videoBanner.isEmpty()) {
        String name = "video_banner_" + System.currentTimeMillis();
        urlVideoBanner = cloudinaryUtils.uploadVideoBanner(videoBanner, name);
      }

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(Map.of("urlVideoBanner", urlVideoBanner));
      JsonObject resp = UnirestUtils.put(urlUpdateVideoBanner, headers, body);
      log.info("Url: {}", urlUpdateVideoBanner);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating slider result: {}", gson.toJson(result));
    return result;
  }


}
