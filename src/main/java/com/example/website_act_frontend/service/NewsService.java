package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.News;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.service.news.AcceptNewsService;
import com.example.website_act_frontend.service.news.CreateNewsService;
import com.example.website_act_frontend.service.news.DeleteNewsService;
import com.example.website_act_frontend.service.news.GetDetailNewsService;
import com.example.website_act_frontend.service.news.GetListNewsService;
import com.example.website_act_frontend.service.news.UpdateNewsService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("NewsService")
public class NewsService {

  private final GetDetailNewsService getDetailNewsService;
  private final GetListNewsService getListNewsService;
  private final CreateNewsService createNewsService;
  private final UpdateNewsService updateNewsService;
  private final DeleteNewsService deleteNewsService;
  private final AcceptNewsService acceptNewsService;

  /**
   * Gọi API để lấy ra danh sách tin tức
   */
  public Map<String, Object> getListNews(Search search, int page, int size) {
    return getListNewsService.execute(search, page, size);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết tin tức
   */
  public Map<String, Object> getDetailNews(int newsId) {
    return getDetailNewsService.execute(newsId);
  }

  /**
   * Gọi API để tạo mới tin tức
   */
  public Map<String, Object> createNews(News news, String token) {
    return createNewsService.execute(news, token);
  }

  /**
   * Gọi API để cập nhật tin tức
   */
  public Map<String, Object> updateNews(News news, String token) {
    return updateNewsService.execute(news, token);
  }

  /**
   * Gọi API để xóa tin tức
   */
  public Map<String, Object> deleteNews(int newsId, String token) {
    return deleteNewsService.execute(newsId, token);
  }

  /**
   * Gọi API để duyệt tin tức
   */
  public Map<String, Object> acceptNews(int newsId, String token) {
    return acceptNewsService.execute(newsId, token);
  }
}