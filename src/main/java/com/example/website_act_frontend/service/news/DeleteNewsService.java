package com.example.website_act_frontend.service.news;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteNewsService")
public class DeleteNewsService {

  private final Gson gson;

  @Value(value = "${url.news.delete}")
  private String urlDeleteNews;

  public Map<String, Object> execute(int newsId, String token) {
    log.info("======== NEWS - DELETE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("newsId", newsId);
      JsonObject resp = UnirestUtils.delete(urlDeleteNews, headers, parameters);
      log.info("Url: {}", urlDeleteNews);
      log.info("News Id: {}", newsId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Deleting news result: {}", gson.toJson(result));
    return result;
  }
}
