package com.example.website_act_frontend.service.news;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.News;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateNewsService")
public class UpdateNewsService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.news.update}")
  private String urlUpdateNews;

  public Map<String, Object> execute(News news, String token) {
    log.info("======== NEWS - UPDATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      if (news.getFile() != null && !news.getFile().isEmpty()) {
        Transformation<?> incoming =
            new Transformation<>().crop("fill").width(160).height(150);
        String newsImg =
            cloudinaryUtils.uploadFile(news.getFile(), "news", "news", incoming);
        if (StringUtils.isNotBlank(newsImg)) {
          news.setImg(newsImg);
        }
      }

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(news);
      JsonObject resp = UnirestUtils.put(urlUpdateNews, headers, body);
      log.info("Url: {}", urlUpdateNews);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating news result: {}", gson.toJson(result));
    return result;
  }
}
