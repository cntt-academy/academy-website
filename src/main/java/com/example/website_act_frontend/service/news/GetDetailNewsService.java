package com.example.website_act_frontend.service.news;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.News;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailNewsService")
public class GetDetailNewsService {

  private final Gson gson;

  @Value(value = "${url.news.getDetail}")
  private String urlGetDetailNews;

  public Map<String, Object> execute(int newsId) {
    log.info("======== News - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, Object> parameters = Map.of("newsId", newsId);
      JsonObject resp = UnirestUtils.getWithoutHeader(urlGetDetailNews, parameters);
      log.info("Url: {}", urlGetDetailNews);
      log.info("News Id: {}", newsId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        News news = gson.fromJson(data.get("news"), News.class);
        result.put("state", "success");
        result.put("news", news);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail news result: {}", gson.toJson(result));
    return result;
  }
}
