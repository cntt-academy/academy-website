package com.example.website_act_frontend.service.news;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.constant.ImageConstant;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.News;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateNewsService")
public class CreateNewsService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.news.create}")
  private String urlCreateNews;

  public Map<String, Object> execute(News news, String token) {
    log.info("======== NEWS - CREATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      String newsImg;
      if (news.getFile() == null || news.getFile().isEmpty()) {
        newsImg = ImageConstant.unknownNewsImage;
      } else {
        Transformation<?> incoming =
            new Transformation<>().crop("fill").width(160).height(150);
        newsImg =
            cloudinaryUtils.uploadFile(news.getFile(), "news", "news", incoming);
        if (StringUtils.isBlank(newsImg)) {
          newsImg = ImageConstant.unknownCourseImage;
        }
      }
      news.setImg(newsImg);

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(news);
      JsonObject resp = UnirestUtils.postByBody(urlCreateNews, headers, body);
      log.info("Url: {}", urlCreateNews);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating news result: {}", gson.toJson(result));
    return result;
  }
}
