package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.service.clazz.CreateClassService;
import com.example.website_act_frontend.service.clazz.DeleteClassService;
import com.example.website_act_frontend.service.clazz.GetDetailClassService;
import com.example.website_act_frontend.service.clazz.GetListClassByAdminEmailService;
import com.example.website_act_frontend.service.clazz.GetListClassByTeacherIdService;
import com.example.website_act_frontend.service.clazz.GetListClassByUserIdService;
import com.example.website_act_frontend.service.clazz.GetListClassService;
import com.example.website_act_frontend.service.clazz.UpdateClassService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("ClassService")
public class ClassService {

  private final GetListClassByAdminEmailService getListClassByAdminEmailService;
  private final GetListClassByTeacherIdService getListClassByTeacherIdService;
  private final GetListClassByUserIdService getListClassByUserIdService;
  private final GetDetailClassService getDetailClassService;
  private final GetListClassService getListClassService;
  private final CreateClassService createClassService;
  private final UpdateClassService updateClassService;
  private final DeleteClassService deleteClassService;

  /**
   * Gọi API để lấy ra danh sách lớp học theo Admin Email
   */
  public Map<String, Object> getListClassByAdminEmail(
      String adminEmail, int page, int size, String token) {
    return getListClassByAdminEmailService.execute(adminEmail, page, size, token);
  }

  /**
   * Gọi API để lấy ra danh sách lớp học theo Teacher Id
   */
  public Map<String, Object> getListClassByTeacherId(
      Integer teacherId, Search search, int page, int size, String token) {
    return getListClassByTeacherIdService.execute(teacherId, search, page, size, token);
  }

  /**
   * Gọi API để lấy ra danh sách lớp học theo User Id
   */
  public Map<String, Object> getListClassByUserId(
      Integer userId, Search search, int page, int size, String token) {
    return getListClassByUserIdService.execute(userId, search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết lớp học
   */
  public Map<String, Object> getDetailClass(int classId, String token) {
    return getDetailClassService.execute(classId, token);
  }

  /**
   * Gọi API để lấy ra danh sách lớp học
   */
  public Map<String, Object> getListClass(Search search, int page, int size) {
    return getListClassService.execute(search, page, size);
  }

  /**
   * Gọi API để thêm mới lớp học
   */
  public Map<String, Object> createClass(Clazz clazz, String token) {
    return createClassService.execute(clazz, token);
  }

  /**
   * Gọi API để cập nhật lớp học
   */
  public Map<String, Object> updateClass(Clazz clazz, String token) {
    return updateClassService.execute(clazz, token);
  }

  /**
   * Gọi API để xóa lớp học
   */
  public Map<String, Object> deleteClass(int classId, String token) {
    return deleteClassService.execute(classId, token);
  }
}
