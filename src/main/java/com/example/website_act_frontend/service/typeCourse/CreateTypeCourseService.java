package com.example.website_act_frontend.service.typeCourse;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.TypeCourse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTypeCourseService")
public class CreateTypeCourseService {

  private final Gson gson;
  @Value(value = "${url.type-course.create}")
  private String urlCreateTypeCourse;

  public Map<String, Object> execute(TypeCourse typeCourse, String token) {
    log.info("======== TYPE COURSE - CREATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(typeCourse);
      JsonObject resp = UnirestUtils.postByBody(urlCreateTypeCourse, headers, body);
      log.info("Url: {}", urlCreateTypeCourse);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating type course result: {}", gson.toJson(result));
    return result;
  }
}
