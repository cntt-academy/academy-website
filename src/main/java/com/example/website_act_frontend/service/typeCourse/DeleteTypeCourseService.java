package com.example.website_act_frontend.service.typeCourse;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteTypeCourseService")
public class DeleteTypeCourseService {

  private final Gson gson;

  @Value(value = "${url.type-course.delete}")
  private String urlDeleteTypeCourse;

  public Map<String, Object> execute(int typeCourseId, String token) {
    log.info("======== TYPE COURSE - DELETE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters = Map.of("typeCourseId", typeCourseId);
      JsonObject resp = UnirestUtils.delete(urlDeleteTypeCourse, headers, parameters);
      log.info("Url: {}", urlDeleteTypeCourse);
      log.info("Type course Id: {}", typeCourseId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Deleting type course result: {}", gson.toJson(result));
    return result;
  }
}
