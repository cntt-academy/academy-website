package com.example.website_act_frontend.service.typeCourse;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.TypeCourse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetDetailTypeCourseService")
public class GetDetailTypeCourseService {

  private final Gson gson;

  @Value(value = "${url.type-course.getDetail}")
  private String urlGetDetailTypeCourse;

  public Map<String, Object> execute(int typeCourseId, String token) {
    log.info("======== TYPE COURSE - GET DETAIL ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> parameters = Map.of("typeCourseId", typeCourseId);
      JsonObject resp = UnirestUtils.get(urlGetDetailTypeCourse, headers, parameters);
      log.info("Url: {}", urlGetDetailTypeCourse);
      log.info("Type course Id: {}", typeCourseId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        TypeCourse typeCourse = gson.fromJson(data.get("typeCourse"), TypeCourse.class);
        result.put("state", "success");
        result.put("typeCourse", typeCourse);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting detail type course result: {}", gson.toJson(result));
    return result;
  }
}
