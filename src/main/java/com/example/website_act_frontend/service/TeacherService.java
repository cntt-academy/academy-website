package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.User;
import com.example.website_act_frontend.service.teacher.CreateTeacherService;
import com.example.website_act_frontend.service.teacher.DeleteTeacherService;
import com.example.website_act_frontend.service.teacher.GetListTeacherService;
import com.example.website_act_frontend.service.teacher.GetTeacherInfoService;
import com.example.website_act_frontend.service.teacher.UpdateTeacherService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("TeacherService")
public class TeacherService {

  private final GetListTeacherService getListTeacherService;
  private final GetTeacherInfoService getTeacherInfoService;
  private final CreateTeacherService createTeacherService;
  private final UpdateTeacherService updateTeacherService;
  private final DeleteTeacherService deleteTeacherService;

  /**
   * Gọi API để lấy ra danh sách Giảng viên
   */
  public Map<String, Object> getListTeacher(
      Search search, int page, int size, String token) {
    return getListTeacherService.execute(search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết Giảng viên
   */
  public Map<String, Object> getTeacherInfo(int teacherId, String token) {
    return getTeacherInfoService.execute(teacherId, token);
  }

  /**
   * Gọi API để thêm Giảng viên
   */
  public Map<String, Object> createTeacher(User teacher, String token) {
    return createTeacherService.execute(teacher, token);
  }

  /**
   * Gọi API để cập nhật thông tin Giảng viên
   */
  public Map<String, Object> updateTeacher(User teacher, String token) {
    return updateTeacherService.execute(teacher, token);
  }

  /**
   * Gọi API để xóa Giảng viên
   */
  public Map<String, Object> deleteTeacher(int teacherId, String token) {
    return deleteTeacherService.execute(teacherId, token);
  }
}
