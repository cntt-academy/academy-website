package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("AdminAddStudentService")
public class AdminAddStudentService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.register.registerStudy}")
  private String urlRegister;
  @Value(value = "${url.user.checkExists}")
  private String urlCheckUserExists;

  public Map<String, Object> execute(String userEmail, Integer classId, String adminEmail) {
    log.info("======== REGISTER - ADMIN ADD STUDENT ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Bước 1: Kiểm tra học viên đã có tài khoản trong hệ thống chưa ?
      Map<String, Object> parameters = Map.of("email", aesUtils.encrypt(userEmail));
      JsonObject respCheckUser =
          UnirestUtils.getWithoutHeader(urlCheckUserExists, parameters);
      log.info("Url: {}", urlCheckUserExists);
      log.info("Parameters: {}", gson.toJson(parameters));
      log.info("Result calling API: {}", respCheckUser);

      if (HttpStatus.OK.value() != respCheckUser.get("status").getAsInt()) {
        log.error("<Error> => {}", respCheckUser.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", respCheckUser.get("message").getAsString());

        log.info("Registration result: {}", gson.toJson(result));
        return result;
      }

      // Bước 2: Thêm học viên vào lớp học ?
      Map<String, Object> body =
          Map.of("userEmail", userEmail,
              "classId", classId,
              "adminEmail", adminEmail);
      JsonObject respRegister =
          UnirestUtils.postByBodyWithoutHeader(urlRegister, gson.toJson(body));
      log.info("Url: {}", urlRegister);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", respRegister);

      if (HttpStatus.OK.value() == respRegister.get("status").getAsInt()) {
        result.put("state", "success");
        result.put("message", "Thêm học viên vào lớp học thành công");
      } else {
        log.error("<Error> => {}", respRegister.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", respRegister.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Admin add student result: {}", gson.toJson(result));
    return result;
  }
}
