package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.StatisticTime;
import com.example.website_act_frontend.model.StudentStatistic;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("OverviewStudentStatisticService")
public class OverviewStudentStatisticService {

  private final GenericMapper genericMapper;
  private final Gson gson;
  @Value(value = "${url.register.overviewStatistic}")
  private String urlOverviewStudentStatistic;

  public Map<String, Object> execute(StatisticTime time, String token) {
    log.info("======== REGISTER - OVERVIEW STUDENT STATISTIC ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> body = new HashMap<>();
      body.put("fromDate", time.getFromDate().format(BackendUtils.formatter));
      body.put("toDate", time.getToDate().format(BackendUtils.formatter));

      JsonObject resp = UnirestUtils.postByBody(urlOverviewStudentStatistic, headers,
          gson.toJson(body));
      log.info("Url: {}", urlOverviewStudentStatistic);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
      } else {
        JsonObject data = resp.get("data").getAsJsonObject();
        List<?> studentStatistics = gson.fromJson(data.get("studentStatistics"), List.class);
        List<StudentStatistic> listStudentStatistics =
            genericMapper.mapToListOfType(studentStatistics, StudentStatistic.class);
        listStudentStatistics.forEach(i -> result.put(i.getCreatedDate(), i.getTotalStudent()));
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting overview student statistic result: {}", gson.toJson(result));
    return result;
  }
}
