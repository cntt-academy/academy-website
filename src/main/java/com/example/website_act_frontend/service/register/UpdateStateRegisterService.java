package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateStateRegisterService")
public class UpdateStateRegisterService {

  private final Gson gson;
  @Value(value = "${url.register.updateState}")
  private String urlUpdateStateRegister;

  public Map<String, Object> execute(Integer registerId, String state, String token) {
    log.info("======== REGISTER - UPDATE STATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> body = Map.of("registerId", registerId, "state", state);
      JsonObject resp = UnirestUtils.put(urlUpdateStateRegister, headers, gson.toJson(body));
      log.info("Url: {}", urlUpdateStateRegister);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating state or register result: {}", gson.toJson(result));
    return result;
  }
}
