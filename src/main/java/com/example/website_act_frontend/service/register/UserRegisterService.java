package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Register;
import com.example.website_act_frontend.service.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UserRegisterService")
public class UserRegisterService {

  private final UserService userService;
  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.register.registerStudy}")
  private String urlRegister;
  @Value(value = "${url.class.checkExists}")
  private String urlCheckClassExists;
  @Value(value = "${url.admin.checkExists}")
  private String urlCheckAdminExists;
  @Value(value = "${url.user.checkExists}")
  private String urlCheckUserExists;

  public Map<String, Object> execute(Register register) {
    log.info("======== REGISTER - USER REGISTER ========");
    log.info("Request: {}", gson.toJson(register));
    Map<String, Object> result = new HashMap<>();

    try {
      // Bước 1: Kiểm tra email học viên có phải của Quản trị viên ?
      Map<String, Object> parametersAdmin = Map.of(
          "email", aesUtils.encrypt(register.getStudent().getEmail()));
      JsonObject respCheckAdmin =
          UnirestUtils.getWithoutHeader(urlCheckAdminExists, parametersAdmin);

      log.info("Url: {}", urlCheckAdminExists);
      log.info("Parameters: {}", gson.toJson(parametersAdmin));
      log.info("Result calling API: {}", respCheckAdmin);

      if (HttpStatus.OK.value() == respCheckAdmin.get("status").getAsInt()) {
        log.error("<Error> => {}", respCheckAdmin.get("message").getAsString());
        result.put("state", "fail");
        result.put("message",
            "Đã có lỗi xảy ra <br> Xin hãy liên hệ với Quản lý trung tâm để được hỗ trợ");

        log.info("Registration result: {}", gson.toJson(result));
        return result;
      }

      // Bước 2: Kiểm tra lớp học có tồn tại hay không ?
      Map<String, Object> body = new HashMap<>();
      body.put("classId", register.getClassId());
      body.put("courseId", register.getCourseId());
      body.put("howToLearn", register.getHowToLearn());

      JsonObject respCheckClass =
          UnirestUtils.postByBodyWithoutHeader(urlCheckClassExists, gson.toJson(body));
      log.info("Url: {}", urlCheckClassExists);
      log.info("Parameters: {}", gson.toJson(body));
      log.info("Result calling API: {}", respCheckClass);

      if (HttpStatus.OK.value() == respCheckClass.get("status").getAsInt()) {
        result.put("state", "success");
        result.put("message", "Xin hãy kiếm tra email để xác nhận thông tin đăng ký");
      } else {
        log.error("<Error> => {}", respCheckClass.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", "Xin hãy liên hệ với Quản trị viên để được hỗ trợ");
      }

      CompletableFuture.runAsync(() -> {
        // Bước 3: Kiểm tra học viên đã có tài khoản trong hệ thống chưa ?
        Map<String, Object> parametersUser =
            Map.of("email", aesUtils.encrypt(register.getStudent().getEmail()));
        JsonObject respCheckUser =
            UnirestUtils.getWithoutHeader(urlCheckUserExists, parametersUser);
        log.info("Url: {}", urlCheckUserExists);
        log.info("Parameters: {}", gson.toJson(parametersUser));
        log.info("Result calling API: {}", respCheckUser);

        // Chưa có => Tạo tài khoản
        boolean userExisted = true;
        if (HttpStatus.OK.value() != respCheckUser.get("status").getAsInt()) {
          userExisted = false;
          Map<String, Object> resultCreateUser = userService.createUser(register.getStudent());
          if ("success".equals(resultCreateUser.get("state"))) {
            userExisted = true;
          }
        }

        // Bước 4: Thêm học viên vào lớp học ?
        if (userExisted) {
          Map<String, Object> bodyRegister = new HashMap<>();
          bodyRegister.put("classId", register.getClassId());
          bodyRegister.put("courseId", register.getCourseId());
          bodyRegister.put("howToLearn", register.getHowToLearn());
          bodyRegister.put("userEmail", register.getStudent().getEmail());
          JsonObject respRegister =
              UnirestUtils.postByBodyWithoutHeader(urlRegister, gson.toJson(bodyRegister));
          log.info("Url: {}", urlRegister);
          log.info("Body: {}", gson.toJson(bodyRegister));
          log.info("Result calling API: {}", respRegister);
        }
      });
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Registration result: {}", gson.toJson(result));
    return result;
  }
}
