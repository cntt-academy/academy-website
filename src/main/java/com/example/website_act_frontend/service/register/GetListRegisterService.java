package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.PageRequest;
import com.example.website_act_frontend.model.RegisterOutput;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("GetListRegisterService")
public class GetListRegisterService {

  private final GenericMapper genericMapper;
  private final Gson gson;

  @Value(value = "${url.register.getList}")
  private String urlGetListRegister;

  public Map<String, Object> execute(Integer classId, int page, int size, String token) {
    log.info("======== REGISTER - GET LIST ========");
    Map<String, Object> result = new HashMap<>();
    result.put("registers", new ArrayList<>());
    result.put("totalElements", 0);
    result.put("totalPages", 0);

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> body = new HashMap<>();
      body.put("classId", classId);
      body.put("pageRequest", new PageRequest(page, size, "createdDate", true));

      JsonObject resp = UnirestUtils.postByBody(urlGetListRegister, headers, gson.toJson(body));
      log.info("Url: {}", urlGetListRegister);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
      } else {
        JsonObject data = resp.get("data").getAsJsonObject();
        List<?> registers = gson.fromJson(data.get("registers"), List.class);
        if (!CollectionUtils.isEmpty(result)) {
          result.replace("registers",
              genericMapper.mapToListOfType(registers, RegisterOutput.class));
          result.replace("totalElements", data.get("totalElements").getAsInt());
          result.replace("totalPages", data.get("totalPages").getAsInt());
        }
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting list register result: {}", gson.toJson(result));
    return result;
  }
}
