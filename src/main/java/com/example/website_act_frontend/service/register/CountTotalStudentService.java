package com.example.website_act_frontend.service.register;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.StatisticTime;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CountTotalStudentService")
public class CountTotalStudentService {

  private final Gson gson;

  @Value(value = "${url.register.countTotal}")
  private String urlCountTotalStudent;

  public Map<String, Object> execute(StatisticTime time, String token) {
    log.info("======== REGISTER - COUNT TOTAL STUDENT ========");
    Map<String, Object> result = new HashMap<>();
    result.put("totalStudent", 0);

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> body = new HashMap<>();
      body.put("fromDate", time.getFromDate().format(BackendUtils.formatter));
      body.put("toDate", time.getToDate().format(BackendUtils.formatter));

      JsonObject resp = UnirestUtils.postByBody(urlCountTotalStudent, headers, gson.toJson(body));
      log.info("Url: {}", urlCountTotalStudent);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
      } else {
        JsonObject data = resp.get("data").getAsJsonObject();
        result.replace("totalStudent", data.get("totalStudent").getAsInt());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Counting total student result: {}", gson.toJson(result));
    return result;
  }
}
