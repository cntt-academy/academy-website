package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Register;
import com.example.website_act_frontend.model.StatisticTime;
import com.example.website_act_frontend.service.register.AdminAddStudentService;
import com.example.website_act_frontend.service.register.CountTotalStudentService;
import com.example.website_act_frontend.service.register.DeleteRegisterService;
import com.example.website_act_frontend.service.register.DetailStudentStatisticService;
import com.example.website_act_frontend.service.register.GetListRegisterService;
import com.example.website_act_frontend.service.register.OverviewStudentStatisticService;
import com.example.website_act_frontend.service.register.UpdateStateRegisterService;
import com.example.website_act_frontend.service.register.UserRegisterService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("RegisterService")
public class RegisterService {

  private final OverviewStudentStatisticService overviewStudentStatisticService;
  private final DetailStudentStatisticService detailStudentStatisticService;
  private final UpdateStateRegisterService updateStateRegisterService;
  private final CountTotalStudentService countTotalStudentService;
  private final GetListRegisterService getListRegisterService;
  private final AdminAddStudentService adminAddStudentService;
  private final DeleteRegisterService deleteRegisterService;
  private final UserRegisterService registerService;

  /**
   * Gọi API để lấy ra tổng quan thống kê học viên
   */
  public Map<String, Object> overviewStatistic(StatisticTime time, String token) {
    return overviewStudentStatisticService.execute(time, token);
  }

  /**
   * Gọi API để lấy ra chi tiết thống kê học viên
   */
  public Map<String, Object> detailStatistic(StatisticTime time, int page, int size, String token) {
    return detailStudentStatisticService.execute(time, page, size, token);
  }

  /**
   * Gọi API để đếm tổng số học viên
   */
  public Map<String, Object> countTotalStudent(StatisticTime time, String token) {
    return countTotalStudentService.execute(time, token);
  }

  /**
   * Gọi API để lấy ra danh sách đăng ký học
   */
  public Map<String, Object> getListRegister(Integer classId, int page, int size, String token) {
    return getListRegisterService.execute(classId, page, size, token);
  }

  /**
   * Gọi API để quản trị viên thêm học viên vào lớp học
   */
  public Map<String, Object> adminAddStudent(String userEmail, Integer classId, String adminEmail) {
    return adminAddStudentService.execute(userEmail, classId, adminEmail);
  }

  /**
   * Gọi API để học viên đăng ký học
   */
  public Map<String, Object> register(Register register) {
    return registerService.execute(register);
  }

  /**
   * Gọi API để cập nhật trạng thái đăng ký học
   */
  public Map<String, Object> updateStateRegister(Integer registerId, String state, String token) {
    return updateStateRegisterService.execute(registerId, state, token);
  }

  /**
   * Gọi API để xóa thông tin đăng ký học
   */
  public Map<String, Object> deleteRegister(Integer registerId, String token) {
    return deleteRegisterService.execute(registerId, token);
  }
}
