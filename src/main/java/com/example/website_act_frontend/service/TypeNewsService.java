package com.example.website_act_frontend.service;

import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.TypeNews;
import com.example.website_act_frontend.service.typeNews.CreateTypeNewsService;
import com.example.website_act_frontend.service.typeNews.DeleteTypeNewsService;
import com.example.website_act_frontend.service.typeNews.GetDetailTypeNewsService;
import com.example.website_act_frontend.service.typeNews.GetListTypeNewsService;
import com.example.website_act_frontend.service.typeNews.UpdateTypeNewsService;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("TypeNewsService")
public class TypeNewsService {

  private final GetDetailTypeNewsService getDetailTypeNewsService;
  private final GetListTypeNewsService getListTypeNewsService;
  private final CreateTypeNewsService createTypeNewsService;
  private final UpdateTypeNewsService updateTypeNewsService;
  private final DeleteTypeNewsService deleteTypeNewsService;

  /**
   * Gọi API để lấy ra danh sách loại tin tức
   */
  public Map<String, Object> getListTypeNews(Search search, int page, int size,
      String token) {
    return getListTypeNewsService.execute(search, page, size, token);
  }

  /**
   * Gọi API để lấy ra thông tin chi tiết loại tin tức
   */
  public Map<String, Object> getDetailTypeNews(int typeNewsId, String token) {
    return getDetailTypeNewsService.execute(typeNewsId, token);
  }

  /**
   * Gọi API để tạo mới loại tin tức
   */
  public Map<String, Object> createTypeNews(TypeNews typeNews, String token) {
    return createTypeNewsService.execute(typeNews, token);
  }

  /**
   * Gọi API để cập nhật loại tin tức
   */
  public Map<String, Object> updateTypeNews(TypeNews typeNews, String token) {
    return updateTypeNewsService.execute(typeNews, token);
  }

  /**
   * Gọi API để xóa loại khóa học
   */
  public Map<String, Object> deleteTypeNews(int typeNewsId, String token) {
    return deleteTypeNewsService.execute(typeNewsId, token);
  }
}
