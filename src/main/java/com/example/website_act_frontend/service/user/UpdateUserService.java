package com.example.website_act_frontend.service.user;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("UpdateUserService")
public class UpdateUserService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.user.update}")
  private String urlUpdateUser;

  public Map<String, Object> execute(User user, String token) {
    log.info("======== USER - UPDATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Set ảnh đại diện khóa học
      if (user.getFile() != null && !user.getFile().isEmpty()) {
        Transformation<?> incoming =
            new Transformation<>()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        String[] email = user.getEmail().split("@");
        String avatar =
            cloudinaryUtils.uploadFile(user.getFile(), "user", email[0], incoming);
        if (StringUtils.isNotBlank(avatar)) {
          user.setAvatar(avatar);
        }
      }

      user.setDob(BackendUtils.formatter.format(user.getDobLocalDate()));

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(user);
      JsonObject resp = UnirestUtils.put(urlUpdateUser, headers, body);
      log.info("Url: {}", urlUpdateUser);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Updating user result: {}", gson.toJson(result));
    return result;
  }
}
