package com.example.website_act_frontend.service.user;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.constant.ImageConstant;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateUserService")
public class CreateUserService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.user.create}")
  private String urlCreateUser;

  public Map<String, Object> execute(User user) {
    log.info("======== USER - CREATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Set ảnh đại diện
      String avatar;
      if (user.getFile() == null || user.getFile().isEmpty()) {
        avatar = ImageConstant.unknownUserImage;
      } else {
        Transformation<?> incoming =
            new Transformation<>()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        String[] email = user.getEmail().split("@");
        avatar = cloudinaryUtils.uploadFile(user.getFile(), "user", email[0], incoming);
        if (StringUtils.isBlank(avatar)) {
          avatar = ImageConstant.unknownUserImage;
        }
      }
      user.setAvatar(avatar);
      user.setDob(BackendUtils.convertStrDate(user.getDob()));

      String body = gson.toJson(user);
      JsonObject resp = UnirestUtils.postByBodyWithoutHeader(urlCreateUser, body);
      user.setPassword(null);
      log.info("Url: {}", urlCreateUser);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
        result.put("message", "Thêm học viên thành công");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating user result: {}", gson.toJson(result));
    return result;
  }
}
