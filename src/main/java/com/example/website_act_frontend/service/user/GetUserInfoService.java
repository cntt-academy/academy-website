package com.example.website_act_frontend.service.user;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetUserInfoService")
public class GetUserInfoService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.user.getById}")
  private String urlGetById;

  public Map<String, Object> execute(int userId, String token) {
    log.info("======== USER - GET INFO ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters =
          Map.of("userId", aesUtils.encrypt(String.valueOf(userId)));
      JsonObject resp = UnirestUtils.get(urlGetById, headers, parameters);
      log.info("Url: {}", urlGetById);
      log.info("User Id: {}", userId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        User user = gson.fromJson(data.get("user"), User.class);
        user.setDobLocalDate(LocalDate.parse(user.getDob(), BackendUtils.formatter));
        result.put("state", "success");
        result.put("user", user);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting user info result: {}", gson.toJson(result));
    return result;
  }
}
