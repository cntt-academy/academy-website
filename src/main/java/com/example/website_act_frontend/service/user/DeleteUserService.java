package com.example.website_act_frontend.service.user;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("DeleteUserService")
public class DeleteUserService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.user.delete}")
  private String urlDeleteUser;

  public Map<String, Object> execute(Integer userId, String token) {
    log.info("======== USER - DELETE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters =
          Map.of("userId", aesUtils.encrypt(String.valueOf(userId)));
      JsonObject resp = UnirestUtils.delete(urlDeleteUser, headers, parameters);
      log.info("Url: {}", urlDeleteUser);
      log.info("User Id: {}", userId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Deleting user result: {}", gson.toJson(result));
    return result;
  }
}
