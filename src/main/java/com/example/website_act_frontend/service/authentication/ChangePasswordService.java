package com.example.website_act_frontend.service.authentication;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.ChangePassword;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ChangePasswordService")
public class ChangePasswordService {

  private final Gson gson;

  @Value(value = "${url.account.changePassword}")
  private String urlChangePassword;

  public Map<String, Object> execute(ChangePassword request, String token) {
    log.info("======== AUTHENTICATION - CHANGE PASSWORD ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> body = new HashMap<>();
      body.put("oldPassword", request.getOldPassword());
      body.put("newPassword", request.getNewPassword());

      JsonObject resp = UnirestUtils.put(urlChangePassword, headers, gson.toJson(body));
      log.info("Url: {}", urlChangePassword);
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Changing password result: {}", gson.toJson(result));
    return result;
  }
}
