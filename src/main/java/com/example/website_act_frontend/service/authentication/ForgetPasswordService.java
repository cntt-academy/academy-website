package com.example.website_act_frontend.service.authentication;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("ForgetPasswordService")
public class ForgetPasswordService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.account.forgetPassword}")
  private String urlForgetPassword;

  public Map<String, Object> execute(String email) {
    log.info("======== AUTHENTICATION - FORGET PASSWORD ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, Object> parameters = new HashMap<>();
      parameters.put("email", aesUtils.encrypt(email));
      JsonObject resp = UnirestUtils.getWithoutHeader(urlForgetPassword, parameters);
      log.info("Url: {}", urlForgetPassword);
      log.info("Email: {}", email);
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
      }
      result.put("message", resp.get("message").getAsString());
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Handle forgot password result: {}", gson.toJson(result));
    return result;
  }
}
