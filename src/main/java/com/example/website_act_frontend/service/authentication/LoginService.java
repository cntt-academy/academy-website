package com.example.website_act_frontend.service.authentication;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.Login;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("LoginService")
public class LoginService {

  private final Gson gson;
  @Value(value = "${url.authentication.login}")
  private String urlLogin;

  public Map<String, Object> execute(Login login) {
    log.info("======== AUTHENTICATION - LOGIN ========");
    Map<String, Object> result = new HashMap<>();
    result.put("state", "fail");

    try {
      String email = login.getEmail();
      log.info("Email: {}", email);
      String password = login.getPassword();
      log.info("Password is not null: {}", isNotBlank(password));
      if (isBlank(email) || isBlank(password)) {
        log.error("<Error> => Request login không hợp lệ");
        result.put("message", "Xin hãy kiểm tra lại tên đăng nhập và mật khẩu");
        return result;
      }

      Map<String, Object> body = new HashMap<>();
      body.put("username", email);
      body.put("password", password);
      JsonObject resp = UnirestUtils.postByBodyWithoutHeader(urlLogin, gson.toJson(body));
      log.info("Url: {}", urlLogin);
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("message", resp.get("message").getAsString());
        return result;
      }

      JsonObject data = resp.get("data").getAsJsonObject();

      String token = data.get("token").getAsString();
      int expiry = data.get("expiry").getAsInt();
      Cookie cookie = BackendUtils.createTokenCookie(token, expiry);

      result.replace("state", "success");
      result.put("role", data.get("role").getAsString());
      result.put("cookie", cookie);
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Login result: {}", gson.toJson(result));
    return result;
  }
}
