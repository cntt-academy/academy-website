package com.example.website_act_frontend.service.teacher;

import com.cloudinary.Transformation;
import com.example.website_act_frontend.common.constant.ImageConstant;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.CloudinaryUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("CreateTeacherService")
public class CreateTeacherService {

  private final CloudinaryUtils cloudinaryUtils;
  private final Gson gson;
  @Value(value = "${url.teacher.create}")
  private String urlCreateTeacher;

  public Map<String, Object> execute(User teacher, String token) {
    log.info("======== TEACHER - CREATE ========");
    Map<String, Object> result = new HashMap<>();

    try {
      // Set ảnh đại diện
      String avatar;
      if (teacher.getFile() == null || teacher.getFile().isEmpty()) {
        avatar = ImageConstant.unknownUserImage;
      } else {
        Transformation<?> incoming =
            new Transformation<>()
                .gravity("face")
                .height(500)
                .width(500)
                .crop("crop")
                .chain()
                .radius("max")
                .chain()
                .width(100)
                .crop("scale");
        String[] email = teacher.getEmail().split("@");
        avatar = cloudinaryUtils.uploadFile(teacher.getFile(), "teacher", email[0], incoming);
        if (StringUtils.isBlank(avatar)) {
          avatar = ImageConstant.unknownUserImage;
        }
      }
      teacher.setAvatar(avatar);

      Map<String, String> headers = BackendUtils.generateHeaders(token);
      String body = gson.toJson(teacher);
      JsonObject resp = UnirestUtils.postByBody(urlCreateTeacher, headers, body);
      log.info("Url: {}", urlCreateTeacher);
      log.info("Body: {}", body);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        result.put("state", "success");
        result.put("message", "Thêm giảng viên thành công");
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Creating teacher result: {}", gson.toJson(result));
    return result;
  }
}
