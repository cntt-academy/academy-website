package com.example.website_act_frontend.service.teacher;

import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.GenericMapper;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.PageRequest;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Slf4j
@RequiredArgsConstructor
@Service("GetListTeacherService")
public class GetListTeacherService {

  private final GenericMapper genericMapper;
  private final Gson gson;

  @Value(value = "${url.teacher.getList}")
  private String urlGetListTeacher;

  public Map<String, Object> execute(
      Search search, int page, int size, String token) {
    log.info("======== TEACHER - GET LIST ========");
    Map<String, Object> result = new HashMap<>();
    result.put("teachers", new ArrayList<>());
    result.put("totalElements", 0);
    result.put("totalPages", 0);

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);

      Map<String, Object> body = new HashMap<>();
      body.put("search", search.getContent());
      body.put("pageRequest", new PageRequest(page, size, "createdDate", true));

      JsonObject resp = UnirestUtils.postByBody(urlGetListTeacher, headers, gson.toJson(body));
      log.info("Url: {}", urlGetListTeacher);
      log.info("Body: {}", gson.toJson(body));
      log.info("Result calling API: {}", resp);
      if (HttpStatus.OK.value() != resp.get("status").getAsInt()) {
        log.error("<Error> => {}", resp.get("message").getAsString());
      } else {
        JsonObject data = resp.get("data").getAsJsonObject();
        List<?> teachers = gson.fromJson(data.get("teachers"), List.class);
        if (!CollectionUtils.isEmpty(result)) {
          result.replace("teachers", genericMapper.mapToListOfType(teachers, User.class));
          result.replace("totalElements", data.get("totalElements").getAsInt());
          result.replace("totalPages", data.get("totalPages").getAsInt());
        }
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting list teacher result: {}", gson.toJson(result));
    return result;
  }
}
