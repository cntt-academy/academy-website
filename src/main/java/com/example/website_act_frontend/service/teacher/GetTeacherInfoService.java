package com.example.website_act_frontend.service.teacher;

import com.example.website_act_frontend.common.utils.AESUtils;
import com.example.website_act_frontend.common.utils.BackendUtils;
import com.example.website_act_frontend.common.utils.UnirestUtils;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service("GetTeacherInfoService")
public class GetTeacherInfoService {

  private final AESUtils aesUtils;
  private final Gson gson;

  @Value(value = "${url.teacher.getInfo}")
  private String urlGetTeacherInfo;

  public Map<String, Object> execute(int teacherId, String token) {
    log.info("======== TEACHER - GET INFO ========");
    Map<String, Object> result = new HashMap<>();

    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      Map<String, Object> parameters =
          Map.of("teacherId", aesUtils.encrypt(String.valueOf(teacherId)));
      JsonObject resp = UnirestUtils.get(urlGetTeacherInfo, headers, parameters);
      log.info("Url: {}", urlGetTeacherInfo);
      log.info("Teacher Id: {}", teacherId);
      log.info("Result calling API: {}", resp);

      if (HttpStatus.OK.value() == resp.get("status").getAsInt()) {
        JsonObject data = resp.get("data").getAsJsonObject();
        User teacher = gson.fromJson(data.get("teacher"), User.class);
        result.put("state", "success");
        result.put("teacher", teacher);
      } else {
        log.error("<Error> => {}", resp.get("message").getAsString());
        result.put("state", "fail");
        result.put("message", resp.get("message").getAsString());
      }
    } catch (Exception e) {
      BackendUtils.handlerException(result, e);
    }

    log.info("Getting teacher info result: {}", gson.toJson(result));
    return result;
  }
}
