package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RegisterOutput implements Serializable {

  private Integer id;
  private Integer collaboratorId;
  private String collaboratorName;
  private Integer studentId;
  private String studentName;
  private String state;
  private String createdDate;
}
