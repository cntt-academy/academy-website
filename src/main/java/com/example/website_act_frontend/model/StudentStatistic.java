package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class StudentStatistic implements Serializable {

  private Integer classId;
  private String className;
  private String classState;
  private String createdDate;
  private Integer totalStudent;
  private Integer numberNewStudent = 0;
  private Integer numberWaitStudent = 0;
  private Integer numberPayHalfStudent = 0;
  private Integer numberConfirmStudent = 0;
}
