package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Search implements Serializable {

  private String name;
  private String type;
  private String state;
  private String content;
  private Integer typeId;
  private Integer typeIdNot;
  private String howToLearn;
  private Boolean isTopStudent;

  public Search(int typeIdNot, String state) {
    this.typeIdNot = typeIdNot;
    this.state = state;
  }

  public Search(String state, int typeId) {
    this.typeId = typeId;
    this.state = state;
  }
}
