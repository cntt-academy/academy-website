package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class StatisticTime implements Serializable {

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate fromDate;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate toDate;
}
