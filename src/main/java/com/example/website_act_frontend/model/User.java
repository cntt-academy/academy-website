package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class User implements Serializable {

  private Integer id;
  private String avatar;
  private String name;
  private String gender;
  private String dob;
  private String phone;
  private String email;
  private String password;
  private String role;
  private String address;
  private String job;
  private String workplace;
  private Boolean isTopStudent;
  private String feedback;
  private String createdBy;
  private String createdDate;
  private MultipartFile file;
  private String emailAdminCreated;
  private Integer classId;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dobLocalDate;
}
