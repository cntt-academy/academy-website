package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Clazz implements Serializable {

  private Integer id;
  private String avatar;
  private String fullName;
  private String name;
  private String howToLearn;
  private String urlClass;
  private Integer numberStudent;
  private String dateOpening;
  private Integer teacherId;
  private String teacher;
  private String manager;
  private String managerEmail;
  private String supporter;
  private String supporterEmail;
  private Integer courseId;
  private String course;
  private String special;
  private String trialLessonName;
  private String trialLessonUrl;
  private String state;
  private String createdBy;
  private String createdDate;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate dateOpeningLocalDate;
}