package com.example.website_act_frontend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class VideoLecture implements Serializable {

  private Integer id;
  private Integer videoOrder;
  private String name;
  private String desc;
  private String url;
  private Integer classId;
  private String state;
  private String createdBy;
  private String createdDate;
  private MultipartFile file;
}