package com.example.website_act_frontend.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class GenericMapper {

  private final ModelMapper modelMapper;

  public GenericMapper(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  public <S, T> List<T> mapToListOfType(List<S> source, Class<T> targetClass) {
    if (CollectionUtils.isEmpty(source)) {
      return new ArrayList<>();
    }
    return source.stream()
        .map(item -> modelMapper.map(item, targetClass))
        .collect(Collectors.toList());
  }
}
