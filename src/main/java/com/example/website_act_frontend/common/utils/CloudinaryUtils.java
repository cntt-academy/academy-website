package com.example.website_act_frontend.common.utils;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import java.text.Normalizer;
import java.util.Map;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RequiredArgsConstructor
@Component("CloudinaryUtils")
public class CloudinaryUtils {

  private final Cloudinary cloudinary;

  /**
   * Chuẩn hóa file name
   */
  public String formatFileName(String fileName) {
    String temp = Normalizer.normalize(fileName, Normalizer.Form.NFD);
    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
    String name =
        pattern
            .matcher(temp)
            .replaceAll("")
            .toLowerCase()
            .replaceAll(" ", "_")
            .replaceAll("đ", "d");
    return name.toLowerCase() + "_" + System.currentTimeMillis();
  }

  /**
   * Upload file lên Cloudinary
   */
  public String uploadFile(
      MultipartFile file, String folder, String name, Transformation<?> incoming) {
    log.info("Upload file {} into folder {}", name, folder);
    try {
      name = formatFileName(name);
      Map<?, ?> uploadResult = cloudinary.uploader()
          .upload(
              file.getBytes(),
              ObjectUtils.asMap(
                  "resource_type", "auto",
                  "public_id", folder + "/" + name,
                  "transformation", incoming));
      return (String) uploadResult.get("url");
    } catch (Exception e) {
      log.info("Upload file error: {}", e.getMessage());
      return null;
    }
  }

  public String uploadImageSlider(MultipartFile file, String name) {
    log.info("Upload slider {}", name);
    Transformation<?> incoming =
        new Transformation<>().width(1350).height(550).crop("scale").chain();
    try {
      Map<?, ?> uploadResult = this.cloudinary
          .uploader()
          .upload(
              file.getBytes(),
              ObjectUtils.asMap(
                  "resource_type", "auto",
                  "public_id", "media-component/" + name,
                  "transformation", incoming));
      return (String) uploadResult.get("url");
    } catch (Exception e) {
      log.info("Upload slider error: {}", e.getMessage());
      return null;
    }
  }

  public String uploadVideoBanner(MultipartFile file, String name) {
    log.info("Upload video banner {}", name);
    Transformation<?> incoming =
        new Transformation<>().width(1350).height(185).crop("fill").chain();
    try {
      Map<?, ?> uploadResult = this.cloudinary
          .uploader()
          .upload(
              file.getBytes(),
              ObjectUtils.asMap(
                  "resource_type", "auto",
                  "public_id", "media-component/" + name,
                  "transformation", incoming));
      return (String) uploadResult.get("url");
    } catch (Exception e) {
      log.info("Upload file error: {}", e.getMessage());
      return null;
    }
  }
}
