package com.example.website_act_frontend.common.utils;

import static com.example.website_act_frontend.common.utils.RedisUtils.get;
import static com.example.website_act_frontend.common.utils.RedisUtils.hget;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.example.website_act_frontend.common.constant.WebsiteConstant;
import com.example.website_act_frontend.common.enums.RedisEnums;
import com.example.website_act_frontend.common.enums.RoleEnums;
import com.example.website_act_frontend.model.Admin;
import com.example.website_act_frontend.model.Clazz;
import com.example.website_act_frontend.model.Course;
import com.example.website_act_frontend.model.Search;
import com.example.website_act_frontend.model.TypeCourse;
import com.example.website_act_frontend.model.TypeNews;
import com.example.website_act_frontend.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

@Slf4j
@RequiredArgsConstructor
@Component("FrontendUtils")
public class FrontendUtils {

  private final Gson gson;
  @Value(value = "${url.media-component.getMediaComponent}")
  private String urlGetMediaComponent;
  @Value(value = "${url.account.getLoggedInfo}")
  private String getLoggedAccountInfo;
  @Value(value = "${url.type-course.getAll}")
  private String urlGetAllTypeCourse;
  @Value(value = "${url.type-news.getAll}")
  private String urlGetAllTypeNews;
  @Value(value = "${url.admin.getManagerInfo}")
  private String urlGetManagerInfo;
  @Value(value = "${url.teacher.getAll}")
  private String urlGetAllTeacher;
  @Value(value = "${url.course.getAll}")
  private String urlGetAllCourse;
  @Value(value = "${url.admin.getOwnerInfo}")
  private String urlGetOwnerInfo;
  @Value(value = "${url.admin.getAll}")
  private String urlGetAllAdmin;
  @Value(value = "${url.class.getAll}")
  private String urlGetAllClass;

  /**
   * Chuẩn hóa thông báo
   */
  public static String standardizedNotification(String notification) {
    return isNotBlank(notification) ? notification : null;
  }

  /**
   * Tạo danh sách số thứ tự page
   */
  public static List<Integer> getListNumberPage(int page) {
    List<Integer> stt = new ArrayList<>();
    for (int i = 1; i <= page; i++) {
      stt.add(i);
    }
    return stt;
  }

  /**
   * Lấy ra các Media Component
   */
  public Map<String, String> getMediaComponent() {
    Map<String, String> result = new HashMap<>();
    try {
      result.put("video_banner", hget("MEDIA_COMPONENT", "VIDEO_BANNER").toString());
      result.put("slider_01", hget("MEDIA_COMPONENT", "SLIDER_01").toString());
      result.put("slider_02", hget("MEDIA_COMPONENT", "SLIDER_02").toString());
      result.put("slider_03", hget("MEDIA_COMPONENT", "SLIDER_03").toString());
      result.put("slider_04", hget("MEDIA_COMPONENT", "SLIDER_04").toString());
      result.put("slider_05", hget("MEDIA_COMPONENT", "SLIDER_05").toString());
    } catch (Exception e) {
      return setMediaComponent();
    }
    return result;
  }

  /**
   * Call API set giá trị Media Component
   */
  public Map<String, String> setMediaComponent() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetMediaComponent);
      if (resp.get("status").getAsInt() != HttpStatus.OK.value()) {
        return getMediaComponent();
      } else {
        log.error("<Error> => Set media component fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set media component error: {}", e.getMessage());
    }
    return new HashMap<>();
  }

  /**
   * Lấy ra thông tin Quản lý trung tâm
   */
  public Admin getManagerInfo() {
    Admin result;
    try {
      result = new Admin();
      result.setId(Integer.parseInt(hget(WebsiteConstant.MANAGER, RedisEnums.ID.name()).toString()));
      result.setAvatar(hget(WebsiteConstant.MANAGER, RedisEnums.AVATAR.name()).toString());
      result.setName(hget(WebsiteConstant.MANAGER, RedisEnums.NAME.name()).toString());
      result.setGender(hget(WebsiteConstant.MANAGER, RedisEnums.GENDER.name()).toString());
      result.setDob(hget(WebsiteConstant.MANAGER, RedisEnums.DOB.name()).toString());
      result.setDobLocalDate(LocalDate.parse(result.getDob(), BackendUtils.formatter));
      result.setPhone(hget(WebsiteConstant.MANAGER, RedisEnums.PHONE.name()).toString());
      result.setEmail(hget(WebsiteConstant.MANAGER, RedisEnums.EMAIL.name()).toString());
      result.setRole(RoleEnums.Manager.name());
      result.setAddress((String) hget(WebsiteConstant.MANAGER, RedisEnums.ADDRESS.name()));
      result.setJob((String) hget(WebsiteConstant.MANAGER, RedisEnums.JOB.name()));
      result.setWorkplace((String) hget(WebsiteConstant.MANAGER, RedisEnums.WORK_PLACE.name()));
      result.setCreatedBy((String) hget(WebsiteConstant.MANAGER, RedisEnums.CREATED_BY.name()));
      result.setCreatedDate(hget(WebsiteConstant.MANAGER, RedisEnums.CREATED_DATE.name()).toString());
    } catch (Exception e) {
      return setManagerInfo();
    }
    return result;
  }

  /**
   * Call API set thông tin Quản lý trung tâm
   */
  public Admin setManagerInfo() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetManagerInfo);
      if (resp.get("status").getAsInt() != HttpStatus.OK.value()) {
        return getManagerInfo();
      } else {
        log.error("<Error> => Set manager info fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set manager info error: {}", e.getMessage());
    }
    return null;
  }

  /**
   * Lấy ra thông tin Giám đốc trung tâm
   */
  public Admin getOwnerInfo() {
    Admin result;
    try {
      result = new Admin();
      result.setId(Integer.parseInt(hget(WebsiteConstant.OWNER, RedisEnums.ID.name()).toString()));
      result.setAvatar(hget(WebsiteConstant.OWNER, RedisEnums.AVATAR.name()).toString());
      result.setName(hget(WebsiteConstant.OWNER, RedisEnums.NAME.name()).toString());
      result.setGender(hget(WebsiteConstant.OWNER, RedisEnums.GENDER.name()).toString());
      result.setDob(hget(WebsiteConstant.OWNER, RedisEnums.DOB.name()).toString());
      result.setDobLocalDate(LocalDate.parse(result.getDob(), BackendUtils.formatter));
      result.setPhone(hget(WebsiteConstant.OWNER, RedisEnums.PHONE.name()).toString());
      result.setEmail(hget(WebsiteConstant.OWNER, RedisEnums.EMAIL.name()).toString());
      result.setRole(RoleEnums.Owner.name());
      result.setAddress((String) hget(WebsiteConstant.OWNER, RedisEnums.ADDRESS.name()));
      result.setJob((String) hget(WebsiteConstant.OWNER, RedisEnums.JOB.name()));
      result.setWorkplace((String) hget(WebsiteConstant.OWNER, RedisEnums.WORK_PLACE.name()));
      result.setCreatedBy((String) hget(WebsiteConstant.OWNER, RedisEnums.CREATED_BY.name()));
      result.setCreatedDate(hget(WebsiteConstant.OWNER, RedisEnums.CREATED_DATE.name()).toString());
    } catch (Exception e) {
      return setOwnerInfo();
    }
    return result;
  }

  /**
   * Call API set thông tin Giám đốc trung tâm
   */
  public Admin setOwnerInfo() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetOwnerInfo);
      if (resp.get("status").getAsInt() != HttpStatus.OK.value()) {
        return getOwnerInfo();
      } else {
        log.error("<Error> => Set owner info fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set owner info error: {}", e.getMessage());
    }
    return null;
  }

  /**
   * Lấy ra thông tin Người dùng đang đăng nhập
   */
  public User getLoggedAccountInfo(String token, String email, String role) {
    User result;
    try {
      result = new User();
      result.setId(Integer.parseInt(String.valueOf(hget(email, RedisEnums.ID.name()))));
      result.setAvatar(String.valueOf(hget(email, RedisEnums.AVATAR.name())));
      result.setName(String.valueOf(hget(email, RedisEnums.NAME.name())));
      result.setGender(String.valueOf(hget(email, RedisEnums.GENDER.name())));
      result.setDob(String.valueOf(hget(email, RedisEnums.DOB.name())));
      result.setDobLocalDate(LocalDate.parse(result.getDob(), BackendUtils.formatter));
      result.setPhone(String.valueOf(hget(email, RedisEnums.PHONE.name())));
      result.setEmail(email);
      result.setRole(role);
      result.setAddress(String.valueOf(hget(email, RedisEnums.ADDRESS.name())));
      result.setJob(String.valueOf(hget(email, RedisEnums.JOB.name())));
      result.setWorkplace(String.valueOf(hget(email, RedisEnums.WORK_PLACE.name())));
      result.setCreatedBy(String.valueOf(hget(email, RedisEnums.CREATED_BY.name())));
      result.setCreatedDate(String.valueOf(hget(email, RedisEnums.CREATED_DATE.name())));
    } catch (Exception e) {
      return setLoggedAccountInfo(token, email, role);
    }
    return result;
  }

  /**
   * Call API set thông tin Người dùng đang đăng nhập
   */
  public User setLoggedAccountInfo(String token, String email, String role) {
    try {
      Map<String, String> headers = BackendUtils.generateHeaders(token);
      JsonObject resp = UnirestUtils.get(getLoggedAccountInfo, headers);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getLoggedAccountInfo(token, email, role);
      } else {
        log.error("<Error> => Set logged user info fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set logged user info error: {}", e.getMessage());
    }
    return null;
  }

  /**
   * Lấy ra danh sách Type course
   */
  public List<TypeCourse> getAllTypeCourse() {
    List<TypeCourse> typeCourses = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_TYPE_COURSE").toString();
      int size = Integer.parseInt(sizeStr);

      TypeCourse typeCourse;
      String dataTypeCourse;
      for (int i = 1; i <= size; i++) {
        dataTypeCourse = get("TYPE_COURSE_" + i).toString();
        String[] data = dataTypeCourse.split(",");
        typeCourse = new TypeCourse();
        typeCourse.setId(Integer.parseInt(data[0]));
        typeCourse.setName(data[1]);
        typeCourses.add(typeCourse);
      }
    } catch (Exception e) {
      return setAllTypeCourse();
    }
    return typeCourses;
  }

  /**
   * Call API set thông tin danh sách Type course
   */
  public List<TypeCourse> setAllTypeCourse() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTypeCourse);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllTypeCourse();
      } else {
        log.error("<Error> => Set list type course fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set list type course error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Lấy ra danh sách Khóa học
   */
  public List<Course> getAllCourse() {
    List<Course> courses = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_COURSE").toString();
      int size = Integer.parseInt(sizeStr);

      Course course;
      String dataCourse;
      for (int i = 1; i <= size; i++) {
        dataCourse = get("COURSE_" + i).toString();
        String[] data = dataCourse.split(",");
        course = new Course();
        course.setId(Integer.parseInt(data[0]));
        course.setType(data[1]);
        course.setName(data[2]);
        courses.add(course);
      }
    } catch (Exception e) {
      return setAllCourse();
    }
    return courses;
  }

  /**
   * Call API set thông tin danh sách Khóa học
   */
  public List<Course> setAllCourse() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllCourse);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllCourse();
      } else {
        log.error("<Error> => Set all course fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set all course error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Lấy ra danh sách Lớp học
   */
  public List<Clazz> getAllClass() {
    List<Clazz> classes = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_CLASS").toString();
      int size = Integer.parseInt(sizeStr);

      Clazz clazz;
      String dataClass;
      for (int i = 1; i <= size; i++) {
        dataClass = get("CLASS_" + i).toString();
        String[] data = dataClass.split(",");
        clazz = new Clazz();
        clazz.setId(Integer.parseInt(data[0]));
        clazz.setName(data[1]);
        classes.add(clazz);
      }
    } catch (Exception e) {
      return setAllClass();
    }
    return classes;
  }

  /**
   * Call API set thông tin danh sách Lớp học
   */
  public List<Clazz> setAllClass() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllClass);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllClass();
      } else {
        log.error("<Error> => Set all class fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set all class error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Lấy ra danh sách Quản trị viên
   */
  public List<Admin> getAllAdmin() {
    List<Admin> admins = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_ADMIN").toString();
      int size = Integer.parseInt(sizeStr);

      Admin admin;
      String dataAdmin;
      for (int i = 1; i <= size; i++) {
        dataAdmin = get("ADMIN_" + i).toString();
        String[] data = dataAdmin.split(",");
        admin = new Admin();
        admin.setId(Integer.parseInt(data[0]));
        admin.setName(data[1]);
        admin.setEmail(data[2]);
        admin.setRole(RoleEnums.valueOf(data[3]).getValue());
        admins.add(admin);
      }
    } catch (Exception e) {
      return setAllAdmin();
    }
    return admins;
  }

  /**
   * Call API set thông tin danh sách Quản trị viên
   */
  public List<Admin> setAllAdmin() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllAdmin);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllAdmin();
      } else {
        log.error("<Error> => Set all admin fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set all admin error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Lấy ra danh sách Giảng viên
   */
  public List<User> getAllTeacher() {
    List<User> teachers = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_TEACHER").toString();
      int size = Integer.parseInt(sizeStr);

      User teacher;
      String dataTeacher;
      for (int i = 1; i <= size; i++) {
        dataTeacher = get("TEACHER_" + i).toString();
        String[] data = dataTeacher.split(",");
        teacher = new User();
        teacher.setId(Integer.parseInt(data[0]));
        teacher.setName(data[1]);
        teachers.add(teacher);
      }
    } catch (Exception e) {
      return setAllTeacher();
    }
    return teachers;
  }

  /**
   * Call API set thông tin danh sách Giảng viên
   */
  public List<User> setAllTeacher() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTeacher);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllTeacher();
      } else {
        log.error("<Error> => Set all teacher fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set all teacher error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Lấy ra danh sách Type news
   */
  public List<TypeNews> getAllTypeNews() {
    List<TypeNews> listTypeNews = new ArrayList<>();
    try {
      String sizeStr = get("TOTAL_TYPE_NEWS").toString();
      int size = Integer.parseInt(sizeStr);

      TypeNews typeNews;
      String dataTypeNews;
      for (int i = 1; i <= size; i++) {
        dataTypeNews = get("TYPE_NEWS_" + i).toString();
        String[] data = dataTypeNews.split(",");
        typeNews = new TypeNews();
        typeNews.setId(Integer.parseInt(data[0]));
        typeNews.setName(data[1]);
        listTypeNews.add(typeNews);
      }
    } catch (Exception e) {
      return setAllTypeNews();
    }
    return listTypeNews;
  }

  /**
   * Call API set thông tin danh sách Type news
   */
  public List<TypeNews> setAllTypeNews() {
    try {
      JsonObject resp = UnirestUtils.getWithoutHeaderAndParameters(urlGetAllTypeNews);
      if (resp.get("status").getAsInt() == HttpStatus.OK.value()) {
        return getAllTypeNews();
      } else {
        log.error("<Error> => Set list type news fail");
      }
    } catch (Exception e) {
      log.error("<Error> => Set list type news error: {}", e.getMessage());
    }
    return new ArrayList<>();
  }

  /**
   * Set giá trị các model dùng chung trong Page của Guest and User
   */
  @SneakyThrows
  public void setCommonModel(
      HttpServletRequest request, Model model, Search search, String token,
      boolean hasSlider, boolean hasFullCourse) {
    log.info("======== FrontendUtils - SET COMMON MODEL ========");

    CompletableFuture<User> getLoggedUser = CompletableFuture.supplyAsync(() -> {
      User user = null;
      if (StringUtils.isNotBlank(token)) {
        String email = (String) request.getSession().getAttribute("email");
        String role = (String) request.getSession().getAttribute("role");
        user = getLoggedAccountInfo(token, email, role);
      }
      return user;
    });

    CompletableFuture<Map<?, ?>> getMediaComponent =
        CompletableFuture.supplyAsync(this::getMediaComponent);

    CompletableFuture<Admin> getManagerInfo =
        CompletableFuture.supplyAsync(this::getManagerInfo);

    CompletableFuture<List<TypeCourse>> getAllTypeCourse =
        CompletableFuture.supplyAsync(this::getAllTypeCourse);

    CompletableFuture<List<TypeNews>> getAllTypeNews =
        CompletableFuture.supplyAsync(() -> getAllTypeNews().stream()
            .filter(item -> item.getId() != 1)
            .collect(Collectors.toList()));

    CompletableFuture<List<Course>> getAllCourse =
        CompletableFuture.supplyAsync(this::getAllCourse);

    User loggedUser = getLoggedUser.get();
    log.info("=> Logged user info: {}", gson.toJson(loggedUser));
    model.addAttribute("loggedUser", loggedUser);

    Map<?, ?> mediaComponents = getMediaComponent.get();
    log.info("=> Media components: {}", gson.toJson(mediaComponents));
    model.addAttribute("video_banner", mediaComponents.get("video_banner"));
    if (hasSlider) {
      model.addAttribute("slider_01", mediaComponents.get("slider_01"));
      model.addAttribute("slider_02", mediaComponents.get("slider_02"));
      model.addAttribute("slider_03", mediaComponents.get("slider_03"));
      model.addAttribute("slider_04", mediaComponents.get("slider_04"));
      model.addAttribute("slider_05", mediaComponents.get("slider_05"));
    }

    Admin manager = getManagerInfo.get();
    log.info("=> Manager info: {}", gson.toJson(manager));
    model.addAttribute("manager_email", manager.getEmail());
    model.addAttribute("manager_phone", manager.getPhone());

    List<TypeCourse> typeCourses = getAllTypeCourse.get();
    log.info("=> List type course: {}", gson.toJson(typeCourses));
    model.addAttribute("typeCourses", typeCourses);

    List<TypeNews> listTypeNews = getAllTypeNews.get();
    log.info("=> List type news: {}", gson.toJson(listTypeNews));
    model.addAttribute("listTypeNews", listTypeNews);

    if (hasFullCourse) {
      List<Course> courses = getAllCourse.get();
      model.addAttribute("fullCourse", courses);
    }

    if (search == null) {
      search = new Search();
    }
    model.addAttribute("search", search);
  }

  /**
   * Set giá trị model thông tin tài khoản Admin đang login
   */
  @SneakyThrows
  public User setLoggedAccountModel(HttpServletRequest request, Model model,
      String token) {
    log.info("======== FrontendUtils - SET LOGGED ACCOUNT INFO ========");

    CompletableFuture<User> getLoggedAccount = CompletableFuture.supplyAsync(() -> {
      String email = (String) request.getSession().getAttribute("email");
      String role = (String) request.getSession().getAttribute("role");
      return getLoggedAccountInfo(token, email, role);
    });

    User loggedAccount = getLoggedAccount.get();
    if (RoleEnums.User.name().equalsIgnoreCase(loggedAccount.getRole())) {
      model.addAttribute("user", loggedAccount);
    } else {
      model.addAttribute("admin", loggedAccount);
    }
    return loggedAccount;
  }

  /**
   * Lấy ra ngày đầu năm
   */
  public LocalDate getFirstDayOfYear() {
    LocalDate currentDate = LocalDate.now();
    int currentYear = currentDate.getYear();
    return LocalDate.of(currentYear, 1, 1);
  }

  /**
   * Lấy ra số hàng chẵn gần nhất (dùng cho thống kê để lấy ra max y)
   */
  public Integer nearestTens(Collection<?> values) {
    if (!values.isEmpty()) {
      Iterator<Integer> iterator = (Iterator<Integer>) values.iterator();

      int maxNumber = iterator.next();

      while (iterator.hasNext()) {
        int number = iterator.next();

        if (number > maxNumber) {
          maxNumber = number;
        }
      }

      return ((maxNumber + 9) / 10) * 10;
    } else {
      return 10;
    }
  }
}
