package com.example.website_act_frontend.common.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("BackendUtils")
public class BackendUtils {

  public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

  /**
   * Tạo headers call API
   */
  public static Map<String, String> generateHeaders(String token) {
    Map<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json");
    headers.put("Authorization", "Bearer " + token);
    return headers;
  }

  /**
   * Tạo cookie lưu token
   */
  public static Cookie createTokenCookie(String token, int expiry) {
    Cookie cookie = new Cookie("token", token);
    cookie.setPath("/");
    cookie.setHttpOnly(true);
    cookie.setSecure(true);
    cookie.setMaxAge(expiry);
    return cookie;
  }

  /**
   * Convert string date về định dạng dd-MM-yyyy
   */
  @SneakyThrows
  public static String convertStrDate(String dateStr) {
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    Date date = sdf1.parse(dateStr);
    SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
    return sdf2.format(date);
  }

  /**
   * Xử lý ngoại lệ trả về khi xử lý Logic lỗi
   */
  public static void handlerException(Map<String, Object> result, Exception e) {
    log.error("<Error> => {}", e.getMessage());
    e.printStackTrace();
    result.put("state", "fail");
    result.put("message", "Đã có lỗi xảy ra. Xin hãy thử lại");
  }

  /**
   * Lấy ra token từ Cookie
   */
  public String getToken(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals("token")) {
          return cookie.getValue();
        }
      }
    }
    return null;
  }

  /**
   * Set giá trị Attribute khi xác thực thất bại
   */
  public void setFailure(HttpServletRequest request, HttpServletResponse response,
      String notification) throws IOException {
    request.getSession().setAttribute("email", null);
    request.getSession().setAttribute("role", null);
    request.getSession().setAttribute("notification", notification);
    response.sendRedirect("/authentication/login");
  }
}
