package com.example.website_act_frontend.common.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.Map;
import java.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UnirestUtils {

  private static final Gson gson = new Gson();

  private static JsonObject appendStatusInObject(String strObject, int status) {
    JsonObject jsonObject = gson.fromJson(strObject, JsonObject.class);
    jsonObject.addProperty("statusCode", status);
    return jsonObject;
  }

  public static String post(String url, Map<String, String> headers) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var5) {
      log.error("Post method request has failed. {}", var5.getMessage());
      var5.printStackTrace();
    }

    return result;
  }

  public static String post(String url, Map<String, String> headers, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var7) {
      log.error("Post method request has failed. {}", var7.getMessage());
      var7.printStackTrace();
    }

    return result;
  }

  public static String post(String url, Map<String, String> headers, long connectionTimeout,
      long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      log.error("Post method request has failed. {}", var9.getMessage());
      var9.printStackTrace();
    }

    return result;
  }

  public static String post(String url, Map<String, String> headers, long connectionTimeout,
      long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var11) {
      log.error("Post method request has failed. {}", var11.getMessage());
      var11.printStackTrace();
    }

    return result;
  }

  public static String post(String url) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var4) {
      log.error("Post method request has failed. {}", var4.getMessage());
      var4.printStackTrace();
    }

    return result;
  }

  public static String post(String url, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var6) {
      log.error("Post method request has failed. {}", var6.getMessage());
      var6.printStackTrace();
    }

    return result;
  }

  public static String post(String url, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var8) {
      log.error("Post method request has failed. {}", var8.getMessage());
      var8.printStackTrace();
    }

    return result;
  }

  public static String post(String url, long connectionTimeout, long socketTimeout,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var10) {
      log.error("Post method request has failed. {}", var10.getMessage());
      var10.printStackTrace();
    }

    return result;
  }

  public static JsonObject postByBody(String url, Map<String, String> headers, String object) {
    JsonObject result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).body(object)
          .asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var6) {
      log.error("Post method request has failed. {}", var6.getMessage());
      var6.printStackTrace();
    }

    return result;
  }

  public static String postByBody(String url, Map<String, String> headers, String object,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers).body(object)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).body(object)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var8) {
      log.error("Post method request has failed. {}", var8.getMessage());
      var8.printStackTrace();
    }

    return result;
  }

  public static String postByBody(String url, Map<String, String> headers, String object,
      long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).body(object)
          .asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      log.error("Post method request has failed. {}", var10.getMessage());
      var10.printStackTrace();
    }

    return result;
  }

  public static String postByBody(String url, Map<String, String> headers, String object,
      long connectionTimeout, long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers).body(object)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).body(object)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var12) {
      log.error("Post method request has failed. {}", var12.getMessage());
      var12.printStackTrace();
    }

    return result;
  }

  public static JsonObject postByBodyWithoutHeader(String url, String object) {
    JsonObject result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/json").body(object).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
    } catch (Exception var5) {
      log.error("Post method request has failed. {}", var5.getMessage());
      var5.printStackTrace();
    }

    return result;
  }

  public static String postByBodyWithoutHeader(String url, String object, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/json").body(object)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
            .header("Content-Type", "application/json").body(object).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var7) {
      log.error("Post method request has failed. {}", var7.getMessage());
      var7.printStackTrace();
    }

    return result;
  }

  public static String postByBodyWithoutHeader(String url, String object, long connectionTimeout,
      long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/json").body(object).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      log.error("Post method request has failed. {}", var9.getMessage());
      var9.printStackTrace();
    }

    return result;
  }

  public static String postByBodyWithoutHeader(String url, String object, long connectionTimeout,
      long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/json").body(object)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
            .header("Content-Type", "application/json").body(object).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var11) {
      log.error("Post method request has failed. {}", var11.getMessage());
      var11.printStackTrace();
    }

    return result;
  }

  public static String postByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var6) {
      log.error("Post method request has failed. {}", var6.getMessage());
      var6.printStackTrace();
    }

    return result;
  }

  public static String postByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers)
            .queryString(parameters).asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers)
            .queryString(parameters).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var8) {
      log.error("Post method request has failed. {}", var8.getMessage());
      var8.printStackTrace();
    }

    return result;
  }

  public static String postByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      log.error("Post method request has failed. {}", var10.getMessage());
      var10.printStackTrace();
    }

    return result;
  }

  public static String postByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters, long connectionTimeout, long socketTimeout,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers)
            .queryString(parameters).asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers)
            .queryString(parameters).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var12) {
      log.error("Post method request has failed. {}", var12.getMessage());
      var12.printStackTrace();
    }

    return result;
  }

  public static String postByParamsWithoutHeader(String url, Map<String, Object> parameters) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/json").queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var5) {
      log.error("Post method request has failed. {}", var5.getMessage());
      var5.printStackTrace();
    }

    return result;
  }

  public static String postByParamsWithoutHeader(String url, Map<String, Object> parameters,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/json").queryString(parameters)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
            .header("Content-Type", "application/json").queryString(parameters).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var7) {
      log.error("Post method request has failed. {}", var7.getMessage());
      var7.printStackTrace();
    }

    return result;
  }

  public static String postByParamsWithoutHeader(String url, Map<String, Object> parameters,
      long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/json").queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      log.error("Post method request has failed. {}", var9.getMessage());
      var9.printStackTrace();
    }

    return result;
  }

  public static String postByParamsWithoutHeader(String url, Map<String, Object> parameters,
      long connectionTimeout, long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/json").queryString(parameters)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
            .header("Content-Type", "application/json").queryString(parameters).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var11) {
      log.error("Post method request has failed. {}", var11.getMessage());
      var11.printStackTrace();
    }

    return result;
  }

  public static String postByForms(String url, Map<String, String> headers,
      Map<String, Object> fields) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).fields(fields)
          .asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var6) {
      log.error("Post method request has failed. {}", var6.getMessage());
      var6.printStackTrace();
    }

    return result;
  }

  public static String postByForms(String url, Map<String, String> headers,
      Map<String, Object> fields, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers).fields(fields)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).fields(fields)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var8) {
      log.error("Post method request has failed. {}", var8.getMessage());
      var8.printStackTrace();
    }

    return result;
  }

  public static String postByForms(String url, Map<String, String> headers,
      Map<String, Object> fields, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).fields(fields)
          .asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      log.error("Post method request has failed. {}", var10.getMessage());
      var10.printStackTrace();
    }

    return result;
  }

  public static String postByForms(String url, Map<String, String> headers,
      Map<String, Object> fields, long connectionTimeout, long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url).headers(headers).fields(fields)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url).headers(headers).fields(fields)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var12) {
      log.error("Post method request has failed. {}", var12.getMessage());
      var12.printStackTrace();
    }

    return result;
  }

  public static String postByFormsWithoutHeader(String url, Map<String, Object> fields) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/x-www-form-urlencoded").fields(fields).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var5) {
      log.error("Post method request has failed. {}", var5.getMessage());
      var5.printStackTrace();
    }

    return result;
  }

  public static String postByFormsWithoutHeader(String url, Map<String, Object> fields,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/x-www-form-urlencoded").fields(fields)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var7) {
      log.error("Post method request has failed. {}", var7.getMessage());
      var7.printStackTrace();
    }

    return result;
  }

  public static String postByFormsWithoutHeader(String url, Map<String, Object> fields,
      long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
          .header("Content-Type", "application/x-www-form-urlencoded").fields(fields).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      log.error("Post method request has failed. {}", var9.getMessage());
      var9.printStackTrace();
    }

    return result;
  }

  public static String postByFormsWithoutHeader(String url, Map<String, Object> fields,
      long connectionTimeout, long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.post(url)
            .header("Content-Type", "application/x-www-form-urlencoded").fields(fields)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url)
            .header("Content-Type", "application/x-www-form-urlencoded").fields(fields).asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var11) {
      log.error("Post method request has failed. {}", var11.getMessage());
      var11.printStackTrace();
    }

    return result;
  }

  public static String put(String url, Map<String, String> headers, String object,
      long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).body(object).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      var10.printStackTrace();
    }

    return result;
  }

  public static String put(String url, Map<String, String> headers, String object,
      long connectionTimeout, long socketTimeout, boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.put(url).headers(headers)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).body(object)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var12) {
      var12.printStackTrace();
    }

    return result;
  }

  public static JsonObject put(String url, Map<String, String> headers, String object) {
    JsonObject result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).body(object).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
    } catch (Exception var6) {
      var6.printStackTrace();
    }

    return result;
  }

  public static String put(String url, Map<String, String> headers, String object,
      boolean... isAsync) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      if (isAsync.length > 0 && Boolean.TRUE.equals(isAsync[0])) {
        Future<HttpResponse<JsonNode>> future = Unirest.put(url).headers(headers)
            .asJsonAsync(new Callback<>() {
              public void failed(UnirestException e) {
                UnirestUtils.log.error("Request has failed. {}",
                    e.getMessage());
                e.printStackTrace();
              }

              public void completed(HttpResponse<JsonNode> response) {
                UnirestUtils.log.info("Request succeeded.");
              }

              public void cancelled() {
                UnirestUtils.log.warn("Request has been canceled.");
              }
            });
        HttpResponse<JsonNode> response = future.get();
        JsonObject jsonObject = appendStatusInObject(response.getBody().toString(),
            response.getStatus());
        result = jsonObject.toString();
      } else {
        HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).body(object)
            .asJson();
        JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
            jsonResponse.getStatus());
        result = jsonObject.toString();
      }
    } catch (Exception var8) {
      var8.printStackTrace();
    }

    return result;
  }

  public static String putByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      var10.printStackTrace();
    }

    return result;
  }

  public static String putByParams(String url, Map<String, String> headers,
      Map<String, Object> parameters) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var6) {
      var6.printStackTrace();
    }

    return result;
  }

  public static String putByForms(String url, Map<String, String> headers,
      Map<String, Object> fields, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).fields(fields)
          .asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      var10.printStackTrace();
    }

    return result;
  }

  public static String putByForms(String url, Map<String, String> headers,
      Map<String, Object> fields) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.put(url).headers(headers).fields(fields)
          .asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var6) {
      var6.printStackTrace();
    }

    return result;
  }

  public static String get(String url, Map<String, String> headers, long connectionTimeout,
      long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      var9.printStackTrace();
    }

    return result;
  }

  public static JsonObject get(String url, Map<String, String> headers) {
    JsonObject result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var5) {
      var5.printStackTrace();
    }

    return result;
  }

  public static String get(String url, Map<String, String> headers, Map<String, Object> parameters,
      long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      var10.printStackTrace();
    }

    return result;
  }

  public static JsonObject get(String url, Map<String, String> headers,
      Map<String, Object> parameters) {
    JsonObject result = null;
    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers)
          .queryString(parameters).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var6) {
      var6.printStackTrace();
    }
    return result;
  }

  public static JsonObject getWithoutHeader(String url, Map<String, Object> parameters) {
    JsonObject result = null;
    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
          .header("Content-Type", "application/json")
          .queryString(parameters).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var6) {
      var6.printStackTrace();
    }
    return result;
  }

  public static JsonObject getWithoutHeaderAndParameters(String url) {
    JsonObject result = null;
    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
          .header("Content-Type", "application/json").asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var6) {
      var6.printStackTrace();
    }
    return result;
  }

  public static String delete(String url, Map<String, String> headers, long connectionTimeout,
      long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.delete(url).headers(headers).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var9) {
      var9.printStackTrace();
    }

    return result;
  }

  public static String delete(String url, Map<String, String> headers) {
    String result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.delete(url).headers(headers).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var5) {
      var5.printStackTrace();
    }

    return result;
  }

  public static String delete(String url, Map<String, String> headers,
      Map<String, Object> parameters, long connectionTimeout, long socketTimeout) {
    String result = null;

    try {
      Unirest.setTimeouts(connectionTimeout, socketTimeout);
      HttpResponse<JsonNode> jsonResponse = Unirest.delete(url).headers(headers)
          .queryString(parameters).asJson();
      JsonObject jsonObject = appendStatusInObject(jsonResponse.getBody().toString(),
          jsonResponse.getStatus());
      result = jsonObject.toString();
    } catch (Exception var10) {
      var10.printStackTrace();
    }

    return result;
  }

  public static JsonObject delete(String url, Map<String, String> headers, Map<String, Object> parameters) {
    JsonObject result = null;

    try {
      Unirest.setTimeouts(60000L, 30000L);
      HttpResponse<JsonNode> jsonResponse = Unirest.delete(url).headers(headers).queryString(parameters).asJson();
      result = appendStatusInObject(jsonResponse.getBody().toString(), jsonResponse.getStatus());
    } catch (Exception var6) {
      var6.printStackTrace();
    }

    return result;
  }
}
