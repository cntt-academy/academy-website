package com.example.website_act_frontend.common.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component("RedisUtils")
public class RedisUtils {

  private static final Gson gson = new Gson();
  private static RedisUtils redisUtils;
  private RedisTemplate redisTemplate;

  public RedisUtils(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  public static Set<String> keys(String key) {
    return redisUtils.redisTemplate.keys(key);
  }

  public static Object get(String key) {
    return redisUtils.redisTemplate.opsForValue().get(key);
  }

  public static void set(String key, String value) {
    redisUtils.redisTemplate.opsForValue().set(key, value);
  }

  public static void set(String key, String value, Integer expire) {
    redisUtils.redisTemplate.opsForValue().set(key, value, (long) expire, TimeUnit.SECONDS);
  }

  public static void delete(String key) {
    redisUtils.redisTemplate.opsForValue().getOperations().delete(key);
  }

  public static void hset(String key, String hashKey, Object object) {
    redisUtils.redisTemplate.opsForHash().put(key, hashKey, object);
  }

  public static void hset(String key, String hashKey, Object object, Integer expire) {
    redisUtils.redisTemplate.opsForHash().put(key, hashKey, object);
    redisUtils.redisTemplate.expire(key, (long) expire, TimeUnit.SECONDS);
  }

  public static void hset(String key, HashMap<String, Object> map) {
    redisUtils.redisTemplate.opsForHash().putAll(key, map);
  }

  public static void hsetAbsent(String key, String hashKey, Object object) {
    redisUtils.redisTemplate.opsForHash().putIfAbsent(key, hashKey, object);
  }

  public static Object hget(String key, String hashKey) {
    return redisUtils.redisTemplate.opsForHash().get(key, hashKey);
  }

  public static Object hget(String key) {
    return redisUtils.redisTemplate.opsForHash().entries(key);
  }

  public static void deleteKey(String key) {
    redisUtils.redisTemplate.opsForHash().getOperations().delete(key);
  }

  public static Boolean hasKey(String key) {
    return redisUtils.redisTemplate.opsForHash().getOperations().hasKey(key);
  }

  public static Boolean hasKey(String key, String hasKey) {
    return redisUtils.redisTemplate.opsForHash().hasKey(key, hasKey);
  }

  @PostConstruct
  public void init() {
    redisUtils = this;
    redisUtils.redisTemplate = this.redisTemplate;
  }

  public <T> void save(String key, Object value, Class<? extends T> inputClass) {
    try {
      this.redisTemplate.opsForHash().put(inputClass.getName(), key, value);
    } catch (Exception var5) {
      var5.printStackTrace();
      log.info("Save data to redis error!,save details: {}", var5.getMessage());
    }

  }

  public <T> T getByKey(String key, Class<? extends T> inputClass) {
    T result = null;

    try {
      Object resp = this.redisTemplate.opsForHash().get(inputClass.getName(), key);
      String resJson = gson.toJson(resp);
      result = gson.fromJson(resJson, inputClass);
    } catch (Exception var6) {
      var6.printStackTrace();
      log.info("Get data from redis error!,getByKey details: {}", var6.getMessage());
    }

    return result;
  }

  public <T> void deleteByKey(String key, Class<? extends T> inputClass) {
    try {
      this.redisTemplate.opsForHash().delete(inputClass.getName(), key);
    } catch (Exception var4) {
      var4.printStackTrace();
      log.info("Delete data from redis error!,deleteByKey details: {}", var4.getMessage());
    }

  }

  public <T> void save(String key, List<T> inputList, Class<? extends T> inputClass) {
    try {
      this.redisTemplate.opsForList().rightPush(key, inputList);
    } catch (Exception var5) {
      var5.printStackTrace();
      log.info("Save data to redis error!,save details: {}", var5.getMessage());
    }

  }

  public <T> List<T> getListByKey(String key) {
    Object result = null;

    try {
      String respJson = gson.toJson(this.redisTemplate.opsForList().leftPop(key));
      return gson.fromJson(respJson, (new TypeToken<List<T>>() {
      }).getType());
    } catch (Exception var4) {
      var4.printStackTrace();
      log.info("Get data from redis error!,getListByKey details: {}", var4.getMessage());
      return (List) result;
    }
  }

  public <T> List<T> getListByKey(String key, Long count) {
    Object result = null;

    try {
      return this.redisTemplate.opsForList().leftPop(key, count);
    } catch (Exception var5) {
      var5.printStackTrace();
      log.info("Get data from redis error!,getListByKey details: {}", var5.getMessage());
      return (List) result;
    }
  }

  public void save(String key, String value) {
    try {
      this.redisTemplate.opsForValue().set(key, value);
    } catch (Exception var4) {
      var4.printStackTrace();
      log.info("Save data to redis error!, details: {}", var4.getMessage());
    }

  }

  public String getByKey(String key) {
    String result = null;

    try {
      result = this.redisTemplate.opsForValue().get(key).toString();
    } catch (Exception var4) {
      var4.printStackTrace();
      log.info("Get data from redis error!, details: {}", var4.getMessage());
    }

    return result;
  }

  public void deleteByKey(String key) {
    try {
      this.redisTemplate.delete(key);
    } catch (Exception var3) {
      var3.printStackTrace();
      log.info("Get data from redis error!, details: {}", var3.getMessage());
    }

  }
}
