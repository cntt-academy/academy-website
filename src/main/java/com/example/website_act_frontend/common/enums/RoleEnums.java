package com.example.website_act_frontend.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleEnums {
  Owner("Giám đốc trung tâm"),
  Manager("Quản lý trung tâm"),
  Collaborator("Cộng tác viên"),
  User("Học viên");
  private final String value;
}
