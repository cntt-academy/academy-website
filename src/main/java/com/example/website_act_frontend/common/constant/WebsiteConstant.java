package com.example.website_act_frontend.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WebsiteConstant {
  public static final String URL_AVATAR_ACADEMY = "https://res.cloudinary.com/dvr6dfixz/image/upload/v1682877256/media-component/logo_ACT.png";
  public static final String URL_VIDEO_BANNER = "https://res.cloudinary.com/dralhpdiv/video/upload/v1649738315/media/Banner.mp4";

  public static final String MANAGER = "MANAGER";
  public static final String OWNER = "OWNER";
}
