package com.example.website_act_frontend.common.constant;

public class ImageConstant {

  public static final String unknownUserImage =
      "https://res.cloudinary.com/dvr6dfixz/image/upload/v1683108415/user/unknown_user.png";

  public static final String unknownCourseImage =
      "https://res.cloudinary.com/dvr6dfixz/image/upload/v1678987892/course/unknown_course.png";

  public static final String unknownNewsImage =
      "https://res.cloudinary.com/dvr6dfixz/image/upload/v1681839758/news/unknow_news.jpg";
}
